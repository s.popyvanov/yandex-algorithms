//
//  September_Problems_Lesson3B_B_WasThereNumberBefore.swift
//  September.Problems.Lesson3B.B.WasThereNumberBefore
//
//  Created by Sergei Popyvanov on 13.10.2021.
//

import XCTest
// Задача В. Встречалось ли число раньше   https://contest.yandex.ru/contest/28964/problems/B/

func parseFromLine() -> [Int] {
    readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
}


enum Answer: String {
    case yes = "YES"
    case no = "NO"
    
    init(_ bool: Bool) {
        self = bool ? .yes : .no
    }
}

// Возвращаемое значеение только для дебага
func wasNumberBefore(_ input: [Int]) -> [String] {
    var set: Set<Int> = Set()
    var answer: [String] = []
    for number in input {
        let isContainCurrentNumber = Answer(set.contains(number)).rawValue
        answer.append(isContainCurrentNumber)
        set.insert(number)
        print("\(isContainCurrentNumber)")
    }
    return answer
}

// Раскомментить
//wasNumberBefore(parseFromLine())

fileprivate struct TestCase {
    let input: [Int]
    let output: [String]
}


final class September_Problems_Lesson3B_B_WasThereNumberBefore: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Вариант из примера
        TestCase(input: [1, 2, 3, 2, 3, 4], output: ["NO", "NO", "NO", "YES", "YES", "NO"]),
        // Краевые
        TestCase(input: [1, 1], output: ["NO", "YES"]),
        TestCase(input: [1, -1, 2, -2], output: ["NO", "NO", "NO", "NO"]),
        TestCase(input: [1], output: ["NO"]),
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = wasNumberBefore(currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testReadLine() {
        let inputReadLine = "1 1"
        let expectedResult = ["NO", "YES"]
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = wasNumberBefore(input)
            XCTAssertEqual(result, expectedResult)
        }
    }

}
