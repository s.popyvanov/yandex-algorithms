//
//  September_Problems_Lesson2_E_Diploms.swift
//  September.Problems.Lesson2.E.Diploms
//
//  Created by Sergei Popyvanov on 15.09.2021.
//

import XCTest

// Задание E. Дипломы в папках https://contest.yandex.ru/contest/28738/problems/E/

func parseFromLine() -> (Int, [Int]) {
    let numberOfFloders = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    let diploms = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (numberOfFloders[0]!, diploms)
}

func calculateMinimumSecondsToFindDiplom(numberOfFolders: Int, diplomsInFolder: [Int]) -> Int {
    guard numberOfFolders > 1 else { return 0 }
    var seconds = 0
    var maxDiplomsInFolder = diplomsInFolder[0]
    for index in 1..<numberOfFolders {
        let currentDiploms = diplomsInFolder[index]
        
        if currentDiploms <= maxDiplomsInFolder {
            seconds += currentDiploms
        } else {
            seconds += maxDiplomsInFolder // предыдущий максимум
            maxDiplomsInFolder = currentDiploms
        }
    }
    return seconds
}

func calculateMinimumSecondsToFindDiplomOptimized(numberOfFolders: Int, diplomsInFolder: [Int]) -> Int {
    guard let maximum = diplomsInFolder.max() else { return 0 }
    let sum = diplomsInFolder.reduce(0, +)
    return sum - maximum
}

// TODO: Раскомментить
//let input = parseFromLine()
//print(calculateMinimumSecondsToFindDiplom(numberOfFolders: input.0, diplomsInFolder: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, [Int])
    let output: Int
}

final class September_Problems_Lesson2_E_Diploms: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Вариант из задания
        TestCase(input: (2, [2, 1]), output: 1),
        // Еще варианты и краевые
        TestCase(input: (2, [1, 2]), output: 1),
        TestCase(input: (3, [2, 3, 1]), output: 3),
        TestCase(input: (3, [10, 1, 1]), output: 2),
        TestCase(input: (3, [1, 1, 1]), output: 2),
        TestCase(input: (3, [10, 10, 10]), output: 20),
        TestCase(input: (3, [1, 2, 3]), output: 3),
        TestCase(input: (3, [3, 2, 1]), output: 3),
        TestCase(input: (6, [10, 5, 1, 7, 2, 9]), output: 24),
        TestCase(input: (6, [2, 5, 1, 7, 10, 9]), output: 24),
        TestCase(input: (1, [56]), output: 0),
        TestCase(input: (3, [1, 3, 2]), output: 3),
        TestCase(input: (25, [36, 25, 11, 35, 6, 19, 37, 26, 21, 2, 44, 24, 38, 39, 5, 18, 13, 15, 42, 50, 7, 31, 10, 9, 33]), output: 546)
    ]
    
    func testReadLine() {
        let inputReadLine = "2\n2 1"
        let expectedResult = 1
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = calculateMinimumSecondsToFindDiplom(
                numberOfFolders: input.0,
                diplomsInFolder: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }
    
    func testMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateMinimumSecondsToFindDiplom(
                            numberOfFolders: currentCase.input.0,
                            diplomsInFolder: currentCase.input.1),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    
    func testMethodOptimized() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateMinimumSecondsToFindDiplomOptimized(
                            numberOfFolders: currentCase.input.0,
                            diplomsInFolder: currentCase.input.1),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
