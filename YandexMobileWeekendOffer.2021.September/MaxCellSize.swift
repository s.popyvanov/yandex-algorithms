//
//  YandexMobileWeekendOffer_2021_September.swift
//  YandexMobileWeekendOffer.2021.September
//
//  Created by Sergei Popyvanov on 24.09.2021.
//

import XCTest

//Имеется табличный layout, разделяющий экран размера h x w горизонтальными и вертикальными линиями. Координаты, вдоль которых проходят горизонтальные и вертикальные линии, заданы в массивах horizontalLinesY[], verticalLinesX[] в произвольном порядке, начало координат - верхний левый угол экрана, от нуля. Необходимо найти максимальную площадь ячейки в таблице, образованной заданными линиями. Толщину линий не следует учитывать при расчете площади ячейки, например, площадь ячейки, образованной двумя горизонтальными линиями с координатами Y, равными 1 и 2, и двумя вертикальными линиями с координатами X, равными 1 и 2, равна 1. Внешние границы экрана также считаются границами ячеек. Так, если экран имеет ширину 2, то считается, что вертикальная линия с координатой X=1 разделяет его на две равные половины, каждая из которых имеет ширину 1.

struct Layout {
    let height: Int
    let width: Int
}

func parseFromLine() -> (layout: Layout, horizontalLines: [Int], verticalLines: [Int]) {
    let layout = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let horizontalLines = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let verticalLines = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (Layout(height: layout[0], width: layout[1]),
            horizontalLines,
            verticalLines)
    
}

func getMaxCellArea(_ layout: Layout, horizontalLinesY: [Int], verticalLinesX: [Int]) -> Int {
    var maxHeight = 0
    var previous = 0
    for lines in horizontalLinesY.sorted() + [layout.height] {
        let height = lines - previous
        previous = lines
        if height > maxHeight { maxHeight = height }
    }
    var maxWidth = 0
    previous = 0
    for lines in verticalLinesX.sorted() + [layout.width] {
        let width = lines - previous
        previous = lines
        if width > maxWidth { maxWidth = width }
    }
    
    return maxHeight * maxWidth
}

//let input = parseFromLine()
//print(getMaxCellArea(
//        input.layout,
//        horizontalLinesY: input.horizontalLines,
//        verticalLinesX: input.verticalLines))

// MARK: - Test

fileprivate struct TestCase {
    let input: ( (Int, Int), [Int], [Int])
    let output: Int
}

final class YandexMobileWeekendOffer_2021_September: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: ((5, 4), [1, 2, 4], [1, 3]), output: 4),
        TestCase(input: ((5, 4), [3, 2, 1], [2, 3]), output: 4),
        // Краевые
        TestCase(input: ((1, 2), [], [1]), output: 1),
        TestCase(input: ((10, 10), [], []), output: 100),
        TestCase(input: ((1, 2), [], [1]), output: 1),
        TestCase(input: ((6, 6), [2, 4, 5], [1, 5]), output: 8),
    ]

    func testMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let input = currentCase.input
            XCTAssertEqual(getMaxCellArea(
                            Layout(height: input.0.0, width: input.0.1),
                            horizontalLinesY: input.1,
                            verticalLinesX: input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "5 4\n1 2 4\n1 3\n"
        let expectedResult = "4"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getMaxCellArea(input.layout, horizontalLinesY: input.horizontalLines, verticalLinesX: input.verticalLines)
            let resultString = "\(result)"
            XCTAssertEqual(resultString, expectedResult)
        }
    }

}
