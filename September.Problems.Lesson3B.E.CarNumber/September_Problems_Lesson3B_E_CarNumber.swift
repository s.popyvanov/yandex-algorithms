//
//  September_Problems_Lesson3B_E_CarNumber.swift
//  September.Problems.Lesson3B.E.CarNumber
//
//  Created by Sergei Popyvanov on 16.10.2021.
//

import XCTest

// Задание Е. Автомобильные номера https://contest.yandex.ru/contest/28964/problems/E/

struct CarFinder {
    private var indications: [String] = []
    private var allSuspects: [String] = []
    
    mutating func addWitnessStatements(_ indication: String) {
        indications.append(indication)
    }
    
    mutating func addSuspectCarNumber(_ carNumber: String) {
        allSuspects.append(carNumber)
    }
    
    var suspects: String {
        var maxCount = 0
        let setIndications = indications.map{ Set(Array($0)) }
        var answer = ""
        for suspect in allSuspects {
            let setSuspect = Set(Array(suspect))
            let count = setIndications.filter { $0.isSubset(of: setSuspect) }.count
            if count == maxCount {
                answer += "\(suspect)\n"
            } else if count > maxCount {
                maxCount = count
                answer = "\(suspect)\n"
            }
        }
        answer = String(answer.dropLast(1))
        
        return answer
    }
}

func getAnswerFromLine() -> String {
    var carFinder = CarFinder()
    var numberOfWitness = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    while numberOfWitness > 0 {
        let indication = readLine()!
        carFinder.addWitnessStatements(indication)
        numberOfWitness -= 1
    }
    var numberOfSuspects = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    while numberOfSuspects > 0 {
        let suspect = readLine()!
        carFinder.addSuspectCarNumber(suspect)
        numberOfSuspects -= 1
    }

    return carFinder.suspects
}

// Раскомментить
//print(getAnswerFromLine())

// МARK - Testing

fileprivate struct TestCase {
    let input: ([String], [String])
    let output: String
}

final class September_Problems_Lesson3B_E_CarNumber: XCTestCase {
    
    var carFinder: CarFinder!
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: (["ABC", "A37", "BCDA"], ["A317BD", "B137AC"]), output: "B137AC"),
        TestCase(input: (["1ABC", "3A4B"], ["A143BC", "C143AB", "AAABC1"]), output: "A143BC\nC143AB"),
    ]

    func testExample() throws {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            carFinder = CarFinder()
            for indication in currentCase.input.0 {
                carFinder.addWitnessStatements(indication)
            }
            for suspect in currentCase.input.1 {
                carFinder.addSuspectCarNumber(suspect)
            }
            XCTAssertEqual(carFinder.suspects, currentCase.output, "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "3\nABDEX\nXBACD\nBAXED\n2\nAAA\nABCD"
        let expectedResult = "AAA\nABCD"
        withStdinReadingString(inputReadLine) {
            let answer = getAnswerFromLine()
            XCTAssertEqual(answer, expectedResult)
        }
    }
}
