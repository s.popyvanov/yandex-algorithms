//
//  September_Problems_Lesson5B_PrefixSums.swift
//  September.Problems.Lesson5B.PrefixSums
//
//  Created by Sergei Popyvanov on 24.10.2021.
//

import XCTest

// Задача A. Префиксные суммы https://contest.yandex.ru/contest/29075/problems/

struct QuerySegment {
    private var prefixSum: [Int] = [0]
    
    init(_ arr: [Int]) {
        var sum = 0
        for number in arr {
            sum += number
            prefixSum.append(sum)
        }
    }
    
    func query(_ start: Int, _ end: Int) -> String {
        String(prefixSum[end] - prefixSum[start - 1])
    }
}

func readQuery() {
    let numberOfQueries = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)[1]
    let inputArray = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let querySegment = QuerySegment(inputArray)
    for _ in 1...numberOfQueries {
        let query = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        print(querySegment.query(query[0], query[1]))
    }
}

// Раскомментить
//readQuery()

// MARK: - Testing

fileprivate struct TestingQuery {
    let query: (Int, Int)
    let expAnswer: String
    
    init(_ query:(Int, Int), _ expAnswer: String) {
        self.query = query
        self.expAnswer = expAnswer
    }
}

fileprivate struct TestCase {
    let input: QuerySegment
    let output: [TestingQuery]
}

final class September_Problems_Lesson5B_PrefixSums: XCTestCase {
    
    fileprivate let testCase = TestCase(
        input: QuerySegment([1,2,3,4]),
        output: [
            TestingQuery((1,1), "1"),
            TestingQuery((1,2), "3"),
            TestingQuery((1,3), "6"),
            TestingQuery((1,4), "10"),])
    

    func testExample() {
        let querySegment = testCase.input
        for query in testCase.output {
            XCTAssertEqual(querySegment.query(query.query.0, query.query.1), query.expAnswer)
        }
    }
    
    func testArrayCount() {
        // Судя по тестам count в Array не вычисляемое свойство, а счетчик, который считается при добавлении переменной
        let arr = (1...50000).map { _ in Int.random(in: 1...1000)}
        let start = CFAbsoluteTimeGetCurrent()
        let _ = arr.count
        let end = CFAbsoluteTimeGetCurrent()
        let _ = arr.count
        let end2 = CFAbsoluteTimeGetCurrent()
        print("diff1 \(end - start), diff2 \(end2 - end)")
    }
}
