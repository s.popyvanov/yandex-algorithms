//
//  September_Problems_Lesson3B_D_GuessNumber.swift
//  September.Problems.Lesson3B.D.GuessNumber
//
//  Created by Sergei Popyvanov on 15.10.2021.
//

import XCTest

// Задача D. Угадай число https://contest.yandex.ru/contest/28964/problems/D/

struct Guesser {
    var answer: String {
        valids.map { $0 }
            .sorted(by: <)
            .map { "\($0)" }
            .joined(separator: " ")
    }
    
    private let maxNumber: Int
    private(set) var valids: Set<Int>
    
    init(_ maxNumber: Int) {
        self.maxNumber = maxNumber
        valids = Set(Array(1...maxNumber))
    }
    
    mutating func addGuessing(_ guessing: [Int], _ isValid: Bool) {
        var newSet: Set<Int> = Set()
        // Использование стандартных функций intersection, subtract - менее эффективно по времени
        for guess in guessing {
            guard valids.contains(guess) else { continue }
            if isValid {
                newSet.insert(guess)
            } else {
                valids.remove(guess)
            }
        }
        if isValid {
            valids = newSet
            return
        }
    }
}

func getAnswerFromLine() -> String {
    let maxNumber = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    var guesser = Guesser(maxNumber)
    var isEnd = false
    while !isEnd {
        let input = readLine()!
        if input == "HELP" {
            isEnd = true
            continue
        }
        
        let guessing = input.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        let validation = readLine()! == "YES"
        guesser.addGuessing(guessing, validation)
    }
    return guesser.answer
}

// Раскомментить
//print(getAnswerFromLine())

// MARK: - Testing

fileprivate struct TestCase {
    let input: (Int, [([Int], Bool)])
    let output: String
}

final class September_Problems_Lesson3B_D_GuessNumber: XCTestCase {

    var guesser: Guesser!
    
    fileprivate let testCases: [TestCase] = [
        // примеры из задания
        TestCase(input: (10,[
                            ([1, 2, 3, 4, 5], true),
                            ([2, 4, 6, 8, 10], false)]),
                 output: "1 3 5"),
        TestCase(input: (10,[
                            ([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], true),
                            ([1], false),
                            ([2], false),
                            ([3], false),
                            ([4], false),
                            ([6], false),
                            ([7], false),
                            ([8], false),
                            ([9], false),
                            ([10], false),]),
                 output: "5"),
        // Краевые
        TestCase(input: (10,[
                            ([10], true)]),
                 output: "10"),
        TestCase(input: (10,[
                            ([10], false)]),
                 output: "1 2 3 4 5 6 7 8 9"),
        TestCase(input: (10,[
                            ([4, 5], true),
                            ([5, 6], true)]),
                 output: "5"),
        TestCase(input: (3,[
                            ([1, 2], false)]),
                 output: "3"),
        TestCase(input: (4,[
                            ([4], false),
                            ([1, 2, 4], true),
                            ([2, 3], true)]),
                 output: "2"),
        TestCase(input: (10,[
                            ([1,2,3,4,5,6], true),
                            ([2,3,4,5,6,7], true),
                            ([2,3,5,9,10], false)]),
                 output: "4 6")
    ]
    
    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            guesser = Guesser(currentCase.input.0)
            for guess in currentCase.input.1 {
                guesser.addGuessing(guess.0, guess.1)
            }
            
            XCTAssertEqual(guesser.answer, currentCase.output, "case number \(index) not passed")
        }
    }
    
    func testIntersectionOfSet() {
        guesser = Guesser(5)
        guesser.addGuessing([1, 2], true)
        guesser.addGuessing([2, 5], true)
        XCTAssertEqual(guesser.valids, Set([2]))
    }
    
    func testReadLine() {
        let inputReadLine = "10\n1\nYES\nHELP"
        let expectedResult = "1"
        withStdinReadingString(inputReadLine) {
            let answer = getAnswerFromLine()
            XCTAssertEqual(answer, expectedResult)
        }
    }
}
