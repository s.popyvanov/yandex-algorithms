//
//  September_Problems_Lesson1A_A_ComplexEquation.swift
//  September.Problems.Lesson1A.A.ComplexEquation
//
//  Created by Sergei Popyvanov on 15.09.2021.
//

import XCTest

// Задание А. Сложное уравнение https://contest.yandex.ru/contest/28724/problems/

func parseFromLine() -> (Int, Int, Int, Int) {
    let a = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
    let b = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
    let c = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
    let d = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
    return (a, b, c, d)
}

// Уравнение необходимо привести ( ax + b ) : ( cx + d ) = 0
// (axc + bc) : (cx + d) = 0
// (axc + bc - ad + ad) : (cx + d) = 0
// (a(cx + d) + bc - ad) : (cx + d) = 0
// a + (bc - ad) / (cx + d) = 0 -> Отсюда условие, что bc != ad при a != 0
// bc - ad = - acx - ad -> x = - b / a 
func getSolutionForEquation(a: Int, b: Int, c: Int, d: Int) -> String {
    switch (a, b, c, d) {
    case (a, b, _, _) where a == 0 && b == 0:
        return "INF"
    case (a, b, c, d) where b * c == a * d && a != 0:
        return "NO"
    case (a, b, _, _) where b % a == 0:
        return "\(-b/a)"
    default:
        return "NO"
    }
}

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, Int, Int, Int)
    let output: String
}

// TODO: раскомментить
//let input = parseFromLine()
//print(getSolutionForEquation(a: input.0, b: input.1, c: input.2, d: input.3))

final class September_Problems_Lesson1A_A_ComplexEquation: XCTestCase {

    fileprivate let testCases: [TestCase] = [
        // примеры из задания
        TestCase(input: (1, 1, 2, 2), output: "NO"),
        TestCase(input: (2, -4, 7, 1), output: "2"),
        TestCase(input: (35, 14, 11, -3), output: "NO"),
        // краевые
        TestCase(input: (0, 0, 2, 2), output: "INF"),
        TestCase(input: (1, 1, 0, 1), output: "-1"),
        
    ]
    
    func testReadLine() {
        let inputReadLine = "1\n1\n2\n2"
        let expectedResult = "NO"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getSolutionForEquation(a: input.0, b: input.1, c: input.2, d: input.3)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let input = currentCase.input
            XCTAssertEqual(getSolutionForEquation(
                            a: input.0,
                            b: input.1,
                            c: input.2,
                            d: input.3),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
