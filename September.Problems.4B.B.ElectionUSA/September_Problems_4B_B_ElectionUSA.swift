//
//  September_Problems_4B_B_ElectionUSA.swift
//  September.Problems.4B.B.ElectionUSA
//
//  Created by Sergei Popyvanov on 17.10.2021.
//

import XCTest

// Задание B. Выборы в США https://contest.yandex.ru/contest/28970/problems/B/
import Foundation // без импорта не скомпилится
struct Vote {
    let candidate: String
    let count: Int
    init(_ candidate: String, _ count: Int) {
        self.candidate = candidate
        self.count = count
    }
}

func countVoices(votes:[Vote]) -> String {
    var voteDict: [String: Int] = [:]
    for vote in votes {
        if voteDict[vote.candidate] == nil {
            voteDict[vote.candidate] = 0
        }
        voteDict[vote.candidate]! +=  vote.count
    }
    var answer = ""
    for candidate in voteDict.keys.sorted() {
        answer += "\(candidate) \(voteDict[candidate]!)\n"
    }
    return String(answer.dropLast(1))
}

func readVotesFromFile() -> [(String, Int)] {
    guard let line = try? String(contentsOfFile: "input.txt") else {
        return []
    }
    let lines = line.split(separator: "\n")
    var result: [(String, Int)] = []
    for line in lines {
        let split = line.split(separator: " ")
        guard split.count == 2 else { continue }
        guard let number = Int(split[1]) else { continue }
        result.append((String(split[0]), number))
    }
    return result
}

// Раскомментить
//let votes = readVotesFromFile().map { Vote($0.0, $0.1)}
//print(countVoices(votes: votes))


// MARK: - Testing

fileprivate struct TestCase {
    let input: [(String, Int)]
    let output: String
}

final class September_Problems_4B_B_ElectionUSA: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // примеры из задания
        TestCase(
            input: [("McCain", 10), ("McCain", 5), ("Obama", 9), ("Obama", 8), ("McCain", 1)],
            output: "McCain 16\nObama 17"),
        TestCase(
            input: [("ivanov", 100), ("ivanov", 500), ("ivanov", 300), ("petr", 70), ("tourist", 1), ("tourist", 2)],
            output: "ivanov 900\npetr 70\ntourist 3"),
        TestCase(
            input: [("bur", 1)],
            output: "bur 1"),
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let votes = currentCase.input.map { Vote($0.0, $0.1)}
            let answer = countVoices(votes: votes)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
}
