//
//  September_Problems_Lesson5B_B_MaxSum.swift
//  September.Problems.Lesson5B.B.MaxSum
//
//  Created by Sergei Popyvanov on 24.10.2021.
//

import XCTest

// B. Максимальная сумма https://contest.yandex.ru/contest/29075/problems/B/

func parseFromLine() -> [Int] {
    let _ = readLine()
    return readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
}

func getMaxSum(_ arr: [Int]) -> String {
    guard !arr.isEmpty else { return ""}
    var maxSum = arr[0]
    
    var firstIndex = 0
    var lastIndex = 0
    
    func moveFirstIndexToPositive(_ firstIndex: inout Int) {
        while firstIndex < arr.count && arr[firstIndex] < 0 {
            maxSum = max(arr[firstIndex], maxSum)
            firstIndex += 1
        }
    }
    
    func sumPositive(_ lastIndex: inout Int) {
        var sum: Int?
        while lastIndex < arr.count && (sum == nil ||  sum! > 0)  {
            if sum == nil {
                sum = arr[lastIndex]
            }
            else {
                sum! += arr[lastIndex]
            }
            maxSum = max(sum!, maxSum)
            lastIndex += 1
        }
    }
    
    while firstIndex < arr.count {
        moveFirstIndexToPositive(&firstIndex)
        lastIndex = firstIndex
        sumPositive(&lastIndex)
        firstIndex = lastIndex
    }
    
    return String(maxSum)
}

// Раскомментить
//let input = parseFromLine()
//print(getMaxSum(input))

// MARK: - Testing

fileprivate struct TestCase {
    let input: [Int]
    let output: String
}

final class September_Problems_Lesson5B_B_MaxSum: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: [1, 2, 3, 4], output: "10"),
        TestCase(input: [5, 4, -10, 4], output: "9"),
        TestCase(input: [450402558, -840167367, -231820501, 586187125, -627664644], output: "586187125"),
        // Краевые
        TestCase(input: [4, -8, -2, 5, -6], output: "5"),
        TestCase(input: [4, -8, -2, 4, -6, 1, 3, 2], output: "6"),
        TestCase(input: [1, -1, 1, -1], output: "1"),
        TestCase(input: [1], output: "1"),
        TestCase(input: [1, -10, 16, 14], output: "30"),
        TestCase(input: [1, -10, 16, -10, 15, 1], output: "22"),
        TestCase(input: [168, -100, 1, 10, 15, 1], output: "168"),
        TestCase(input: [1, -10, 1, 10, -10, 9, 0, 1], output: "11"),
        TestCase(input: [-1, -2, -3, -4], output: "-1"),
        TestCase(input: [-1, 0, 1, -1, -2, -2], output: "1"),
        TestCase(input: [-8], output: "-8")
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = getMaxSum(currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testArrayAppendOrCreate() {
        // Создание массивов. Чуть медленнее первый метод, но эта разница заметна только на больших значениях.
        var arr1 = Array(repeating: 0, count: 1000000)
        
        let start = CFAbsoluteTimeGetCurrent()
        for index in 0..<1000000 {
            arr1[index] = Int.random(in: 1...1000)
        }
        let end = CFAbsoluteTimeGetCurrent()
        
        var arr2: [Int] = []
        for _ in 0..<1000000 {
            arr2.append(Int.random(in: 1...1000))
        }
        let end2 = CFAbsoluteTimeGetCurrent()
        print("Create diff1 \(end - start), Append - diff2 \(end2 - end)")
    }
    
    func testReadLine() {
        let inputReadLine = "4\n1 2 3 4"
        let expectedResult = "10"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getMaxSum(input)
            XCTAssertEqual(result, expectedResult)
        }
    }

}
