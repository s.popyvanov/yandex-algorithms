//
//  September_Problems_B_CircleMetro.swift
//  September.Problems.B.CircleMetro
//
//  Created by Sergei Popyvanov on 05.09.2021.
//

import XCTest

// Задача  B. Кольцевая линия метро  https://contest.yandex.ru/contest/28730/problems/B/

func parseFromLine() -> (Int, Int, Int) {
    let stations = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    return (stations[0]!, stations[1]!, stations[2]!)
}

final class StationNode {
    let number: Int
    weak var previousStation: StationNode?
    var nextStation: StationNode?
    
    init(number: Int) {
        self.number = number
    }
}

struct CircleMetroStation {
    
    let stations: [StationNode]
    
    init(numbers: Int) {
        var stations: [StationNode] = []
        for number in 1...numbers { stations.append(StationNode(number: number)) }
        stations[0].previousStation = stations[numbers - 1]
        stations[numbers - 1].nextStation = stations[0]
        for index in 0..<stations.count {
            let currentStation = stations[index]
            if stations.indices.contains(index + 1) {
                let nextStation = stations[index+1]
                currentStation.nextStation = nextStation
            }
            if stations.indices.contains(index - 1) {
                let previousStation = stations[index - 1]
                currentStation.previousStation = previousStation
            }
        }
        self.stations = stations
    }
    
}

func calculateNumberOfPassedStation(numberOfStation: Int, departureNumber: Int, arrivalNumber: Int) -> Int {
    let circleMetroStation = CircleMetroStation(numbers: numberOfStation)
    let departureStation = circleMetroStation.stations.first{ $0.number == departureNumber}!
    let firstWayCount: Int = {
        var count: Int = 0
        var currentNumberOfStation = departureNumber
        var currentStation = departureStation
        while currentNumberOfStation != arrivalNumber {
            currentStation = currentStation.nextStation!
            currentNumberOfStation = currentStation.number
            if currentNumberOfStation == arrivalNumber { break }
            count += 1
        }
        return count
    }()
    let secondWayCount: Int = {
        var count: Int = 0
        var currentNumberOfStation = departureNumber
        var currentStation = departureStation
        while currentNumberOfStation != arrivalNumber {
            currentStation = currentStation.previousStation!
            currentNumberOfStation = currentStation.number
            if currentNumberOfStation == arrivalNumber { break }
            count += 1
            
        }
        return count
    }()
    return min(firstWayCount, secondWayCount)
}

// TODO: расскомментить
//let input = parseFromLine()
//print(calculateNumberOfPassedStation(
//        numberOfStation: input.0,
//        departureNumber: input.1,
//        arrivalNumber: input.2))

// Оптимизированный вариант из разбора

func calculateNumberOfPassedStationOptimized(numberOfStation: Int, departureNumber: Int, arrivalNumber: Int) -> Int {
    let destination = abs(departureNumber - arrivalNumber) - 1
    return min(destination, abs(numberOfStation - destination) - 2)
}
let input = parseFromLine()
print(calculateNumberOfPassedStationOptimized(
        numberOfStation: input.0,
        departureNumber: input.1,
        arrivalNumber: input.2))

// Test

fileprivate struct TestCase {
    let input: (Int, Int, Int)
    let output: Int
}

final class September_Problems_B_CircleMetro: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: (100, 5, 6), output: 0),
        TestCase(input: (10, 1, 9), output: 1),
        // Краевые
        TestCase(input: (100, 1, 99), output: 1),
        TestCase(input: (99, 98, 99), output: 0),
        TestCase(input: (2, 1, 2), output: 0),
        // Когда в обе стороны одинаковое расстояние
        TestCase(input: (12, 5, 11), output: 5)
    ]
    
    func testReadLine() {
        let inputReadLine = "10 5 6"
        let expectedResult = 0
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = calculateNumberOfPassedStation(
                numberOfStation: input.0,
                departureNumber: input.1,
                arrivalNumber: input.2)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testCalculations() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateNumberOfPassedStation(
                            numberOfStation: currentCase.input.0,
                            departureNumber: currentCase.input.1,
                            arrivalNumber: currentCase.input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    
    func testOptimizedCalculations() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateNumberOfPassedStationOptimized(
                            numberOfStation: currentCase.input.0,
                            departureNumber: currentCase.input.1,
                            arrivalNumber: currentCase.input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }

}
