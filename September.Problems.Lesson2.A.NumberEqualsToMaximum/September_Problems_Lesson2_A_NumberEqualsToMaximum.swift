//
//  September_Problems_Lesson2_NumberEqualsToMaximum.swift
//  September.Problems.Lesson2.NumberEqualsToMaximum
//
//  Created by Sergei Popyvanov on 08.09.2021.
//

// Задача А. Количество равных максимальному https://contest.yandex.ru/contest/28738/problems/

import XCTest

final class MaximumCalculator {
    private(set) var countOfMaximum = 0
    private var maximumNumber = 0
    
    func addNumber(_ number: Int) {
        switch number {
        case number where number < maximumNumber:
            break
        case number where number > maximumNumber:
            maximumNumber = number
            countOfMaximum = 1
        case number where number == maximumNumber:
            countOfMaximum += 1
        default:
            break
        }
    }
}

func parseResultFromLine() -> Int {
    var isExit = false
    let counter = MaximumCalculator()
    while !isExit {
        let number = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
        guard number != 0 else {
            isExit = true
            break
        }
        counter.addNumber(number)
    }
    print(counter.countOfMaximum)
    return counter.countOfMaximum
}

// TODO: раскомментить
// parseResultFromLine()

// MARK: - Test

fileprivate struct TestCase {
    let input: [Int]
    let output: Int
}


final class September_Problems_Lesson2_A_NumberEqualsToMaximum: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: [1,7,9], output: 1),
        TestCase(input: [1,3,3,1], output: 2),
        // Краевые
        TestCase(input: [1], output: 1),
        TestCase(input: [2], output: 1),
        TestCase(input: [1, 100, 100, 200, 999, 999, 999, 2, 5, 8, 7], output: 3),
    ]
    
    private var sut = MaximumCalculator()
    
    func testReadLine() {
        let inputReadLine = "1\n7\n9\n0"
        let expectedResult = 1
        withStdinReadingString(inputReadLine) {
            let output = parseResultFromLine()
            XCTAssertEqual(output, expectedResult)
        }
    }

    func testNumbers() {
        for index in 0..<testCases.count {
            sut = MaximumCalculator()
            let expected = testCases[index].output
            testCases[index].input.forEach { number in
                sut.addNumber(number)
            }
            XCTAssertEqual(sut.countOfMaximum, expected, "case \(index) not passed")
        }
    }
}
