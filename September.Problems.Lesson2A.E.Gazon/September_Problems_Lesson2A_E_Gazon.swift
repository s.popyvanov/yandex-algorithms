//
//  September_Problems_Lesson2A_E_Gazon.swift
//  September.Problems.Lesson2A.E.Gazon
//
//  Created by Sergei Popyvanov on 04.10.2021.
//

import XCTest

// Задание Е. Газон https://contest.yandex.ru/contest/28736/problems/E/
// Не забыть добавить import иначе не заработают математические функции sqareRoot, .roundUp в контесте
import Foundation

struct Point: Equatable {
    let x: Int
    let y: Int
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
}

struct Rectangle: Equatable {
    /// Левая нижняя точка прямоугольника
    let point1: Point
    
    /// Правая верхняя точка прямоугольника
    let point2: Point
    
    init(_ point1: Point, _ point2: Point) {
        self.point1 = Point(min(point1.x, point2.x), min(point1.y, point2.y))
        self.point2 = Point(max(point1.x, point2.x), max(point1.y, point2.y))
    }
    
    /// Квадрат, описывающий окружность
    init(_ circle: Circle) {
        point1 = Point(circle.center.x - circle.radius, circle.center.y - circle.radius)
        point2 = Point(circle.center.x + circle.radius, circle.center.y + circle.radius)
    }
    
    func isPointOfRectangle(_ point: Point) -> Bool {
        (point.x >= point1.x && point.x <= point2.x) && (point.y >= point1.y && point.y <= point2.y)
    }
    
}

struct Circle {
    let center: Point
    let radius: Int

    func isCirclePoint(_ point: Point) -> Bool {
        let dimX = (point.x - center.x) * (point.x - center.x)
        let dimY = (point.y - center.y) * (point.y - center.y)
        return dimX + dimY <= (radius * radius)
    }
}

func intersectionOf(_ rect1: Rectangle, _ rect2: Rectangle) -> Rectangle? {
    let newPoint1 = Point(max(rect1.point1.x, rect2.point1.x), max(rect1.point1.y, rect2.point1.y))
    let newPoint2 = Point(min(rect1.point2.x, rect2.point2.x), min(rect1.point2.y, rect2.point2.y))
    
    guard rect1.isPointOfRectangle(newPoint1) && rect1.isPointOfRectangle(newPoint2) else { return nil }
    return Rectangle(newPoint1, newPoint2)
}

func parseFromLine() -> (Rectangle, Circle) {
    let rect = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let circle = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (Rectangle(Point(rect[0], rect[1]), Point(rect[2], rect[3])),
            Circle(center: Point(circle[0], circle[1]), radius: circle[2]))
}

func calculateTrimmedAndWateredGrass(lawnRect: Rectangle, wateredCircle: Circle) -> Int {
    let wateredRectangle = Rectangle(wateredCircle)
    guard let intersectionRect = intersectionOf(lawnRect, wateredRectangle) else { return 0 }
    var count = 0
    
    for x in intersectionRect.point1.x...intersectionRect.point2.x {
        let deltaY = Double((wateredCircle.radius * wateredCircle.radius) - ((x - wateredCircle.center.x) * (x - wateredCircle.center.x))).squareRoot()
        let yMax = min(Double(intersectionRect.point2.y), ((Double(wateredCircle.center.y) + deltaY).rounded(.down)))
        let yMin = max(Double(intersectionRect.point1.y), (Double(wateredCircle.center.y) - deltaY).rounded(.up))
        count += yMax >= yMin ? Int(yMax - yMin) + 1 : 0
    }
    
    return count
}

// TODO: - раскомментить
//let input = parseFromLine()
//print(calculateTrimmedAndWateredGrass(lawnRect: input.0, wateredCircle: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Rectangle, Circle)
    let output: Int
}

final class September_Problems_Lesson2A_E_Gazon: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Пример из задания
        .init(input: (Rectangle(Point(0, 0), Point(5, 4)), Circle(center: Point(4, 0), radius: 3)), output: 14),
        .init(input: (Rectangle(Point(0, 0), Point(1, 1)), Circle(center: Point(3, 4), radius: 5)), output: 4),
        .init(input: (Rectangle(Point(1200, 4270), Point(1280, 4400)), Circle(center: Point(1234, 4321), radius: 50)), output: 6989),
        .init(input: (Rectangle(Point(-45025, -23515), Point(78543, 43421)), Circle(center: Point(5, -3), radius: 78000)), output: 8022957502),
        .init(input: (Rectangle(Point(50000, 50000), Point(51000, 51000)), Circle(center: Point(55000, 54000), radius: 5300)), output: 91948),
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = calculateTrimmedAndWateredGrass(
                lawnRect: currentCase.input.0,
                wateredCircle: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testRectangle() {
        let rect1 = Rectangle(Point(-2, 3), Point(5, -6))
        XCTAssertEqual(rect1.point1, Point(-2, -6))
        XCTAssertEqual(rect1.point2, Point(5, 3))
        let rect2 = Rectangle(Point(1, 1), Point(1, 2))
        XCTAssertEqual(rect2.point1, Point(1, 1))
        XCTAssertEqual(rect2.point2, Point(1, 2))
        
        XCTAssertTrue(rect2.isPointOfRectangle(Point(1, 1)))
        XCTAssertFalse(rect2.isPointOfRectangle(Point(2, 1)))
    }
    
    func testRectangleIntersection() {
        let rect1 = Rectangle(Point(2, 2), Point(6, 5))
        let rect2 = Rectangle(Point(5, 1), Point(8, 4))
        let expRect1IntersectRect2 = intersectionOf(rect1, rect2)
        XCTAssertEqual(expRect1IntersectRect2!, Rectangle(Point(5, 2), Point(6, 4)))
        let rect3 = Rectangle(Point(0, 0), Point(-5, -5))
        XCTAssertNil(intersectionOf(rect1, rect3))
    }
    
    func testCircle() {
        let circle = Circle(center: Point(0, 0), radius: 2)
        XCTAssertTrue(circle.isCirclePoint(Point(1, 1)))
        XCTAssertTrue(circle.isCirclePoint(Point(2, 0)))
        XCTAssertTrue(circle.isCirclePoint(Point(-2, 0)))
        XCTAssertFalse(circle.isCirclePoint(Point(2, 2)))
        XCTAssertFalse(circle.isCirclePoint(Point(-2, -2)))
        XCTAssertFalse(circle.isCirclePoint(Point(-2, 2)))
        
        let rectFromCircle = Rectangle(circle)
        XCTAssertEqual(rectFromCircle, Rectangle(Point(-2, -2), Point(2, 2)))
    }
    
    func testReadLine() {
        let inputReadLine = "0 0 5 4\n4 0 3"
        let expectedResult = 14
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = calculateTrimmedAndWateredGrass(lawnRect: input.0, wateredCircle: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }
}
