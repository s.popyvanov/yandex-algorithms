//
//  September_Sets_Practice.swift
//  September.Sets.Practice
//
//  Created by Sergei Popyvanov on 12.10.2021.
//

import XCTest

// Задача № 1
//Дана последовательность положительный чисел N и число Х.
// Найти два различных числа А и В из последовательности таких что А + В = Х или вернуть 0, 0 если такой пары нет

//  Вариант 1
// O(N^2) просто перебрать все варианты
func getPairOfSumVar1(_ sequence: [Int], sum: Int) -> (Int, Int) {
    for firstIndex in 0..<sequence.count {
        for secondIndex in 0..<sequence.count {
            guard firstIndex != secondIndex else { continue }
            if sequence[firstIndex] + sequence[secondIndex] == sum {
                return (sequence[firstIndex], sequence[secondIndex])
            }
        }
    }
    
    return (0, 0)
}

//  Вариант 2
// O(N) Создаем множество из исходной последовательности и проверяем
func getPairOfSumVar2(_ sequence: [Int], sum: Int) -> (Int, Int) {
    let set = Set(sequence)
    for number in sequence {
        if set.contains(sum - number) { return (number, sum - number)}
    }
    
    return (0, 0)
}

//Задача № 2
//Дана последовательность из N слов, длина каждого не превосходит K
//В записи каждого из M слов (каждое длиной до К), может быть пропущена одна буква.
//Для каждого слова сказать входит ли оно (даже с пропущенной буквой) в словарь.
//за  O(NK^2 + M)
func filterWordsWithMistake(dict: [String], inputWords: [String]) -> [String]{
    var wordsWithMistakes = Set(dict)
    for word in dict {
        // Создание нового множества занимает N*K^2
        for deletePosition in 0..<word.count {
            var word = word
            let deleteIndex = word.index(word.startIndex, offsetBy: deletePosition)
            word.remove(at: deleteIndex)
            print(word)
            wordsWithMistakes.insert(word)
        }
    }
    var answer: [String] = []
    // Перебор M слов с операцией O(1) проверка вхождения в множество
    for word in inputWords {
        if  wordsWithMistakes.contains(word) {
            answer.append(word)
        }
    }

    return answer
}

fileprivate struct TestCase1 {
    let input: ([Int], Int)
    let output: (Int, Int)
}

fileprivate struct TestCase2 {
    let input: ([String], [String])
    let output: [String]
}


final class September_Sets_Practice: XCTestCase {

    fileprivate let testCases1: [TestCase1] = [
        TestCase1(input: ([1, 2, 5, 6], 3), output: (1, 2)),
        TestCase1(input: ([8, 1, 4, 15, 29], 7), output: (0, 0)),
        TestCase1(input: ([1, 2, 1, 1], 3), output: (1, 2))
    ]
    
    fileprivate let testCases2: [TestCase2] = [
        TestCase2(input: (["abc", "cad", "brabra", "pole"], ["bc", "ca", "brbra", "mca", "cad"]), output: ["bc", "ca", "brbra", "cad"]),
        TestCase2(input: (["a", "ab", "abc", "abcd"], ["b", "ba", "glad", "mis"]), output: ["b"]),
        TestCase2(input: (["a", "ab", "abc", "abcd"], ["biver"]), output: [])
    ]
    
    func testTask1Var1() {
        for index in 0..<testCases1.count {
            let currentCase = testCases1[index]
            let answer = getPairOfSumVar1(currentCase.input.0, sum: currentCase.input.1)
            
            XCTAssertTrue(
                currentCase.output.0 == answer.0 &&
                    currentCase.output.1 == answer.1,
                "case number \(index) not passed")
            
        }
    }
    
    func testTask1Var2() {
        for index in 0..<testCases1.count {
            let currentCase = testCases1[index]
            let answer = getPairOfSumVar2(currentCase.input.0, sum: currentCase.input.1)
            
            XCTAssertTrue(
                currentCase.output.0 == answer.0 &&
                    currentCase.output.1 == answer.1,
                "case number \(index) not passed")
            
        }
    }
    
    func testTask2() {
        for index in 0..<testCases2.count {
            let currentCase = testCases2[index]
            let answer = filterWordsWithMistake(dict: currentCase.input.0, inputWords: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
}
