//
//  AnyNestingArray.swift
//  AnyNestingArray
//
//  Created by Sergei Popyvanov on 30.10.2021.
//

import XCTest

// Задача - развернуть массив любой вложенности, состоящий из любых типов в линейный массив целых чисел
// Если тип не является числом - не выводить

func toArray<T>(_ input: [T]) -> [Int] {
    var output: [Int] = []
    for element in input {
        if let element = element as? Int {
            output.append(element)
            continue
        }
        guard let arrayOfAny = element as? [Any] else { continue }
        output += toArray(arrayOfAny)
    }
    return output
}

final class AnyNestingArray: XCTestCase {

    func testExample() throws {
        XCTAssertEqual(toArray([[1, 2, 3]]), [1, 2, 3])
        XCTAssertEqual(toArray([1, 2, 3]), [1, 2, 3])
        XCTAssertEqual(toArray([[[1]]]), [1])
        XCTAssertEqual(toArray([[1, 2, 3, "sd"], [[4]], nil, "as", [4, 5], [[[6]]]]), [1, 2, 3, 4, 4, 5, 6])
        
    }

}
