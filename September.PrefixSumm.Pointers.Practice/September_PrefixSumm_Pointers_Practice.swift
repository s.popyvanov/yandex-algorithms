//
//  September_PrefixSumm_Pointers_Practice.swift
//  September.PrefixSumm.Pointers.Practice
//
//  Created by Sergei Popyvanov on 22.10.2021.
//

import XCTest
// Практические задания из лекции «Префиксные суммы и два указателя»

// Задание 1. Реализовать подсчет полуинтервала для массива Int

// Построение массива O(N)
func makePrefixSum(_ input: [Int]) -> [Int] {
    var prefixSum: [Int] = [0]
    for index in 1...input.count {
        let sum = prefixSum[index - 1] + input[index - 1]
        prefixSum.append(sum)
    }
    
    return prefixSum
}

func getRangeSum(_ input: [Int], from: Int, to: Int) -> Int {
    let prefixSum = makePrefixSum(input)
    guard input.indices.contains(from) && input.indices.contains(to) else { return 0 }
    return prefixSum[to] - prefixSum[from] // Сам запрос суммы O(1)
}

func getFullRangeSum(_ input: [Int], from: Int, to: Int) -> Int {
    let prefixSum = makePrefixSum(input)
    guard input.indices.contains(from) && input.indices.contains(to) else { return 0 }
    return prefixSum[to + 1] - prefixSum[from]
}

// Задание №2. Посчитать количество нулей на интервале

func getZeroCount(_ input: [Int], from: Int, to: Int) -> Int {
    var prefixZeroCountSum: [Int] = [0]
    for index in 1...input.count { // создание O(N)
        let count = input[index - 1] == 0 ? 1 : 0
        let zeroCountSum = prefixZeroCountSum[index - 1] + count
        prefixZeroCountSum.append(zeroCountSum)
    }
    guard input.indices.contains(from) && input.indices.contains(to) else { return 0 }
    return prefixZeroCountSum[to + 1] - prefixZeroCountSum[from] // O(1)
}

// Заданиее №3. Последовательность N. Посчитать количество отрезков с 0 суммой
// ключевой моменнт, что нулевой отрезок имеет разность одинаковых префиксных сумм

func countZeroIntervals(_ input: [Int]) -> Int {
    var prefixDict: [Int: Int] = [0: 1] // словарь, ключ - сумма, значение - количество с такой суммой
    var currentSum = 0
    for number in input {
        currentSum += number
        if prefixDict[currentSum] == nil {
            prefixDict[currentSum] = 0
        }
        prefixDict[currentSum]! += 1
    }
    print(prefixDict)
    var countZeroRanges = 0
    for key in prefixDict.keys {
        let sum = prefixDict[key]!
        countZeroRanges += Int((sum * (sum - 1) ) / 2) // считается перебор вариантов
    }
    return countZeroRanges
}

//  Задание №4. Дана отсортированная последовательность чисел длинной N и число К
// Необходимо найти количество пар чисел А, В, таких, что B -A > K

// Решение за O(N^2)
func countPairDiff(_ sortedArr: [Int], greater k: Int) -> Int{
    var count = 0
    for leftRangeIndex in 0..<sortedArr.count { // Двигаем левую границу
        for rightRangeIndex in leftRangeIndex..<sortedArr.count { // перебираем правые
            if sortedArr[rightRangeIndex] - sortedArr[leftRangeIndex] > k {
                count += 1
            }
        }
    }
    return count
}

// Решение за O(N)
func countPairDiffOpt(_ sortedArr: [Int], greaterThat k: Int) -> Int {
    var count = 0
    let countOfArr = sortedArr.count
    var rightRangeIndex = 0
    for leftRangeIndex in 0..<countOfArr {
        // Особенность while, если && в условиях и первый false, то второй даже не проверяется.
        while rightRangeIndex < countOfArr
                && sortedArr[rightRangeIndex] - sortedArr [leftRangeIndex] <= k {
            rightRangeIndex += 1 // находим то состояние, когда уже все значения от текущего справа будут удовлетворять условия разность > k
        }
        count += countOfArr - rightRangeIndex
    }
    return count
}

// Задача №5. Суммарный профессионализм команды.
// Из отсортированного списка профессионализма игроков, составить состав сплоченную команду (профессионализм самого сильного игрока не превосходит профессионализм самых слабых игроков).
// И найти максимальное значение

func bestTeamSum(_ players: [Int]) -> Int {
    let teamSize = players.count
    var bestSum = 0
    var currentSum = 0
    var lastIndex = 0
    for firstIndex in 0..<teamSize {
        while lastIndex < teamSize
                && (lastIndex == firstIndex
                    || players[firstIndex] + players[firstIndex + 1] >= players[lastIndex]) {
                    currentSum += players[lastIndex]
                    lastIndex += 1
                }
        bestSum = max(currentSum, bestSum)
        currentSum -= players[firstIndex]
    }
    return bestSum
}

// Задача №6. Слить две отсортированных последовательности в одну.
//

func merge(_ arr1: [Int], _ arr2: [Int]) -> [Int] {
    let arr1Size = arr1.count
    let arr2Size = arr2.count
    var arr1FirstIndex = 0, arr2FirstIndex = 0
    var merged = Array(repeating: 0, count: arr1Size + arr2Size)
    for index in 0..<(arr1Size + arr2Size) {
        if arr1FirstIndex != arr1Size && (arr2FirstIndex == arr2Size || arr1[arr1FirstIndex] < arr2[arr2FirstIndex]) {
            merged[index] = arr1[arr1FirstIndex]
            arr1FirstIndex += 1
        } else {
            merged[index] = arr2[arr2FirstIndex]
            arr2FirstIndex += 1
        }
    }
    return merged
}

final class September_PrefixSumm_Pointers_Practice: XCTestCase {

    func testMakePrefixSum() {
        let input = [1, 2, 5, 2, 4, 6]
        let expected = [0, 1, 3, 8, 10, 14, 20]
        XCTAssertEqual(makePrefixSum(input), expected   )
    }
    
    func testCaclulateRangeSumQuery() {
        let input = [1, 8, 0, 2, 3, 4]
        XCTAssertEqual(getRangeSum(input, from: 0, to: 2), 9)
        XCTAssertEqual(getRangeSum(input, from: 1, to: 2), 8)
        XCTAssertEqual(getRangeSum(input, from: 4, to: 5), 3)
    }
    
    func testCaclulateFullRangeSumQuery() {
        let input = [1, 8, 0, 2, 3, 4]
        XCTAssertEqual(getFullRangeSum(input, from: 0, to: 4), 14)
        XCTAssertEqual(getFullRangeSum(input, from: 0, to: 1), 9)
        XCTAssertEqual(getFullRangeSum(input, from: 1, to: 3), 10)
        XCTAssertEqual(getFullRangeSum(input, from: 1, to: 5), 17)
        XCTAssertEqual(getFullRangeSum(input, from: 0, to: 5), 18)
    }
    
    func testZeroCountPrefixSum() {
        let input = [0, 1, 2, 3, 0, 0, 3, 4, 5, 0]
        XCTAssertEqual(getZeroCount(input, from: 0, to: 1), 1)
        XCTAssertEqual(getZeroCount(input, from: 0, to: 3), 1)
        XCTAssertEqual(getZeroCount(input, from: 0, to: 4), 2)
        XCTAssertEqual(getZeroCount(input, from: 4, to: 9), 3)
        XCTAssertEqual(getZeroCount(input, from: 0, to: 9), 4)
    }
    
    func testCountZeroIntervals() {
        let input1 = [0]
        XCTAssertEqual(countZeroIntervals(input1), 1)
        let input2 = [-1, 1, 3, 5, -3, -5]
        XCTAssertEqual(countZeroIntervals(input2), 3)
    }
    
    func testCountPairDiff() {
        let input = [1, 2, 6, 7, 9]
        XCTAssertEqual(countPairDiff(input, greater: 4), 5)
        XCTAssertEqual(countPairDiffOpt(input, greaterThat: 4), 5)
    }
    
    func testBestTeamSum() {
        let teams1 = [1]
        XCTAssertEqual(bestTeamSum(teams1), 1)
        let teams2 = [1, 1, 2, 3, 3, 6, 8, 9, 20]
        XCTAssertEqual(bestTeamSum(teams2), 29)
        let teams3 = [1, 3]
        XCTAssertEqual(bestTeamSum(teams3), 4)
    }
    
    func testMerge() {
        XCTAssertEqual(merge([2], [1]), [1, 2])
        XCTAssertEqual(merge([1, 2, 3], [2, 3, 4]), [1, 2, 2, 3 ,3, 4])
        
        // тест производительности
        let arr1 = (0...1000).map { _ in Int.random(in: 1...10000)}.sorted()
        let arr2 = (0...1000).map { _ in Int.random(in: 1...10000)}.sorted()
        let startStandartMethod = CFAbsoluteTimeGetCurrent()
        let arr3 = (arr1 + arr2).sorted()
        let endStandartMethod = CFAbsoluteTimeGetCurrent()
        let answer = merge(arr1, arr2)
        let endMethod = CFAbsoluteTimeGetCurrent()
        print("Standart-custom method measure: \(endStandartMethod - startStandartMethod) - \(endMethod - endStandartMethod)")
        XCTAssertEqual(answer, arr3)
    }
    
    func testWhile() {
        func fail() -> Bool {
            assertionFailure()
            return false
        }
        let falseCondition = false
        while falseCondition && fail() { // падает первое условие, а второе даже не проверяется
            assertionFailure()
        }
        
        // Для ИЛИ аналогично, если первый True - второй даже не проверяется.
        while !(!falseCondition || fail()) {
            assertionFailure()
        }
        
        if falseCondition && fail() {
            assertionFailure()
        }
        
        if falseCondition, fail() {
            assertionFailure()
        }
        
        guard falseCondition, fail() else {
            return
        }
        assertionFailure()
    }
}
