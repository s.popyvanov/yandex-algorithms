//
//  September_Problems_Lesson5B_C_ComputerForEach.swift
//  September.Problems.Lesson5B.C.ComputerForEach
//
//  Created by Sergei Popyvanov on 27.10.2021.
//

import XCTest

// Задача C. Каждому по компьютеру https://contest.yandex.ru/contest/29075/problems/C/

func parseFromLine() -> ([Int], [Int]) {
    let _ = readLine()
    let groups = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let rooms = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (groups, rooms)
}

func distributeComputers(studentGroups: [Int], roomsCapacity: [Int]) -> String {
    var answerGroup = ""
    var groupWithRoomCounter = 0
    var groupsNumber = 1
    var notOccupiedRooms: Set<Int> = Set((1...roomsCapacity.count).map{ $0 })
    
    while groupsNumber < studentGroups.count + 1 {
        let studentsInGroupWithTeacher = studentGroups[groupsNumber - 1] + 1
        var freeComputersInRoom: Int?
        var suitableRoomNumber: Int?
        for roomNumber in notOccupiedRooms {
            let roomCapacity = roomsCapacity[roomNumber - 1]
            if roomCapacity >= studentsInGroupWithTeacher {
                if freeComputersInRoom == nil {
                    freeComputersInRoom = roomCapacity - studentsInGroupWithTeacher
                    suitableRoomNumber = roomNumber
                } else if freeComputersInRoom! > roomCapacity - studentsInGroupWithTeacher {
                    freeComputersInRoom = roomCapacity - studentsInGroupWithTeacher
                    suitableRoomNumber = roomNumber
                }
            }
        }
        if let suitableRoomNumber = suitableRoomNumber {
            notOccupiedRooms.remove(suitableRoomNumber)
            answerGroup += "\(suitableRoomNumber)\n"
            groupWithRoomCounter += 1
        } else {
            answerGroup += "0\n"
        }
        groupsNumber += 1
    }
    
    return String("\(groupWithRoomCounter)\n\(answerGroup)".dropLast(1))
}

// Раскомментить
//let input = parseFromLine()
//print(distributeComputers(studentGroups: input.0, roomsCapacity: input.1))

// MARK: - Testing

fileprivate struct TestCase {
    let input: ([Int], [Int])
    let output: String
}

final class September_Problems_Lesson5B_C_ComputerForEach: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: ([1], [2]), output: "1\n1"),
        TestCase(input: ([1], [1]), output: "0\n0"),
        // Краевые, доп примеры
        TestCase(input: ([10, 1, 5], [2, 6, 11]), output: "3\n3\n1\n2"),
        TestCase(input: ([10, 1, 5], [2, 6, 11, 5]), output: "3\n3\n1\n2"),
        TestCase(input: ([10, 1, 5], [2, 5, 6]), output: "2\n0\n1\n3"),
        TestCase(input: ([10, 10, 10], [1, 1, 1]), output: "0\n0\n0\n0"),
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = distributeComputers(
                studentGroups: currentCase.input.0,
                roomsCapacity: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testSetIndexFromArray() {
        let set: Set<Int> = Set((1...[10, 20, 30].count).map{ $0 })
        XCTAssertEqual(set, Set([1, 2, 3]))
    }
    
    func testReadLine() {
        let inputReadLine = "1 1\n1\n2"
        let expectedResult = "1\n1"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = distributeComputers(studentGroups: input.0, roomsCapacity: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }
}
