//
//  September_SortingByCount_Dictionary.swift
//  September.SortingByCount.Dictionary
//
//  Created by Sergei Popyvanov on 16.10.2021.
//

import XCTest

// Практические задачи из лекции Тренировки по алгоритмам от Яндекса. Лекция 4: «Словари и сортировка подсчётом» https://www.youtube.com/watch?v=Nb5mW1yWVSs

// Пример сортировки подсчетом O(4N + M). Если известен диапазон O(2N), если M небольшой диапазон O(2N)

func sortingByCounting(_ input: [Int]) -> [Int] {
    // Сложность операций поиска максимума и минимума O(n), n - количество элементов в массиве
    guard let min = input.min(),
          let max = input.max()
    else { return [] }
    let delta = min < 0 ? abs(min) : 0
    let range = max - min + 1
    // Создание  O(M), где M - величина диапазона + по памяти O(M)
    var countingArray = Array.init(repeating: 0, count: range)
    // Добавление в массив всех значений O(n)
    for number in input {
        countingArray[number + delta] += 1
    }
    // Отостированный массив так же по памяти может распухнуть до O(n)
    var sortingArray: [Int] = []
    // Проход по всем значениям O(n)
    for number in 0..<countingArray.count {
        let count = countingArray[number]
        guard count > 0 else { continue }
        for _ in 1...count {
            sortingArray.append(number - delta)
        }
    }
    return sortingArray
}

// Дано два числа X и Y без ведущих нулей (это когда 0 спереди стоит). Необходимо проверить можно ли получить первое из второго перестановкой цифр

func isDigitPermutation(number1: Int, number2: Int) -> Bool {
    func getCountingArray(_ number: Int) -> [Int] {
        var array = Array(repeating: 0, count: 10)
        var number = number
        while number > 0 {
            let lastDigit = number % 10
            array[lastDigit] += 1
            number /= 10
        }
        return array
    }
    let digitsOfNumber1 = getCountingArray(number1)
    let digitsOfNumber2 = getCountingArray(number2)
    
    return digitsOfNumber1 == digitsOfNumber2
}

// Задача №2 На словари
// На шахматной доске NxN находятся M ладей (бьет клетки на той же оризонтали или вертикали до ближайшей занятой). Определить, сколько пар ладей бьют друг друга. Ладьи задаются парой чисел I и. J,  обозначающие координаты клетки 1<= N <= 10^9, O <= M <= 2 * 10^5
struct Rook {
    let x: Int
    let y: Int
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
}

func countBeatingRooks(_ rooks: [Rook]) -> Int {
    var rooksInX: [Int: Int] = [:]
    var rooksInY: [Int: Int] = [:]
    // Заполняем словарь
    func addRook(_ dict: inout [Int: Int], key: Int) {
        if dict[key] == nil {
            dict[key] = 0
        }
        dict[key]! += 1
    }
    // Считаем пары ладей по горизонталям и вертикалям
    func countPairs(_ dict: [Int: Int]) -> Int {
        var pairs = 0
        dict.keys.forEach { key in
            pairs += dict[key]! - 1
        }
        return pairs
    }
    
    for rook in rooks {
        addRook(&rooksInY, key: rook.y)
        addRook(&rooksInX, key: rook.x)
    }
    return countPairs(rooksInX) + countPairs(rooksInY)
}

// Задача №3 Гистограмма
//Дана строка S. Вывести гистограмму (коды символов отсортированы)
//S = Hello, world!
//         #
//         ##
//############
// !,Hdelorw

func getChart(for string: String) -> String {
    var countDict: [Character: Int] = [:]
    var maxCount = 0
    var answer = ""
    for char in string {
        if countDict[char] == nil {
            countDict[char] = 0
        }
        countDict[char]! += 1
        maxCount = max(maxCount, countDict[char] ?? 0)
    }
    let sortedCharKeys = countDict.keys.sorted() // O(nlogn)
    
    // За счет этого перебора O(n^2) в худшем случае, если много одинаковых символов
    for row in (1...maxCount).reversed() {
        var rowString = ""
        for char in sortedCharKeys {
            rowString.append(countDict[char]! >= row ? "#" : " ")
        }
        answer += rowString + "\n"
    }
    answer.append(sortedCharKeys.map { "\($0)"}.joined())
    return answer
}

// Задача № 4 Оптимизация
//Сгруппировать слова по общим буквам
//Sample Input: ("eat", "tea", "tan", "ate", "nat", "bat")
//Output (("ate", "eat", "tea", ("nat", "tan"), ("bat"))

func groupWords(_ words: [String]) -> [[String]] {
    var groupWords: [String: [String]] = [:]
    for word in words {
        let sortedWord = Array(word).sorted().map{"\($0)"}.joined()
        if groupWords[sortedWord] == nil {
            groupWords[sortedWord] = []
        }
        groupWords[sortedWord]!.append(word)
    }
    var answer: [[String]] = []
    for sortedWords in groupWords {
        answer.append(sortedWords.value)
    }
    
    return answer
}

final class September_SortingByCount_Dictionary: XCTestCase {

    func testSortingByCounting() {
        for _ in 0...100 {
            let input = (0...1000).map({ _ in Int.random(in: -1000...1000)})
            let extendedResult = input.sorted(by: <)
            let answer = sortingByCounting(input)
            XCTAssertEqual(extendedResult, answer)
        }
    }
    
    func testPermutations() {
        let inputs: [(Int, Int)] = [ (2021, 1202), (901, 203), (11, 11), (556, 655)]
        let outputs: [Bool] = [true, false, true, true]
        for (index, input )in inputs.enumerated() {
            let expected = outputs[index]
            let answer = isDigitPermutation(number1: input.0, number2: input.1)
            XCTAssertEqual(answer, expected)
        }
    }
    
    func testRooks() {
        let input1: [Rook] = [Rook(1, 1), Rook(2, 2), Rook(3, 3)]
        let expectedAnswer1 = 0
        let answer1 = countBeatingRooks(input1)
        XCTAssertEqual(answer1, expectedAnswer1)
        
        let input2: [Rook] = [Rook(1, 1), Rook(1, 2), Rook(3, 3)]
        let expectedAnswer2 = 1
        let answer2 = countBeatingRooks(input2)
        XCTAssertEqual(answer2, expectedAnswer2)
    }
    
    func testChart() {
        let input = "cbba"
        let expectedResult = " # \n###\nabc"
        let answer = getChart(for: input)
        print(getChart(for: "Hello, world!"))
        XCTAssertEqual(expectedResult, answer)
    }
    
    func testSortedWords() {
        let input = ["eat", "tea", "tan", "ate", "nat", "bat"]
        print(groupWords(input))
    }
}
