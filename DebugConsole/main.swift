//
//  main.swift
//  DebugConsole
//
//  Created by Sergei Popyvanov on 17.10.2021.
//

import Foundation

//  Копируем сюда код для проверки - и проверяем решения из примеров или примеры от чат-бота в консоле после запуска проекта
// Ниже пример проекта, который можно удалять

struct CarFinder {
    private var indications: [String] = []
    private var allSuspects: [String] = []
    
    mutating func addWitnessStatements(_ indication: String) {
        indications.append(indication)
    }
    
    mutating func addSuspectCarNumber(_ carNumber: String) {
        allSuspects.append(carNumber)
    }
    
    var suspects: String {
        var maxCount = 0
        let setIndications = indications.map{ Set(Array($0)) }
        var answer = ""
        for suspect in allSuspects {
            let setSuspect = Set(Array(suspect))
            let count = setIndications.filter { $0.isSubset(of: setSuspect) }.count
            if count == maxCount {
                answer += "\(suspect)\n"
            } else if count > maxCount {
                maxCount = count
                answer = "\(suspect)\n"
            }
        }
        answer = String(answer.dropLast(1))
        
        return answer
    }
}

func getAnswerFromLine() -> String {
    var carFinder = CarFinder()
    var numberOfWitness = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    while numberOfWitness > 0 {
        let indication = readLine()!
        carFinder.addWitnessStatements(indication)
        numberOfWitness -= 1
    }
    var numberOfSuspects = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    while numberOfSuspects > 0 {
        let suspect = readLine()!
        carFinder.addSuspectCarNumber(suspect)
        numberOfSuspects -= 1
    }

    return carFinder.suspects
}

print(getAnswerFromLine())

