//
//  September_Problems_Lessson2A_B_Petya.swift
//  September.Problems.Lessson2A.B.Petya
//
//  Created by Sergei Popyvanov on 02.10.2021.
//

import XCTest

// Задание B. Изобретательный Петя https://contest.yandex.ru/contest/28736/problems/B/

func parseFromLine() -> (String, String) {
    let firstLine = readLine()!.split(separator: Character(" ")).map(String.init)
    let secondLine = readLine()!.split(separator: Character(" ")).map(String.init)
    return (firstLine[0], secondLine[0])
}

/// Вспомогательный метод, которые проверяет насколько строка output пересекается со строкой output (см. тесты ниже)
func mixStrings(repeatedText: String, output: String) -> String {
    var repeated = repeatedText
    while repeated.count < output.count {
        repeated += repeatedText
    }
    
    let count = min(repeated.count, output.count - 1)
    var intersectionCount = 0
    for offset in 1...count {
        let repeatString = repeated[repeated.index(repeated.endIndex, offsetBy: -offset)..<repeated.endIndex]
        let outputString = output[output.startIndex..<output.index(output.startIndex, offsetBy: offset)]
        if repeatString == outputString { intersectionCount = offset }
    }
    return String(output[output.index(output.startIndex, offsetBy: intersectionCount)..<output.endIndex])
}


func getTextToPrint(repeatText: String, output: String) -> String {
    var repeated = repeatText
    while repeated.count < output.count {
        repeated += repeatText
    }
    
    var answer = ""
    var offset = -1
    // Ищем в output первый символ с конца, который равняется последнему символу repeat
    while repeated[repeated.index(repeated.endIndex, offsetBy: -1)] != output[output.index(output.endIndex, offsetBy: offset)]
            && abs(offset) < output.count {
        answer.insert(output[output.index(output.endIndex, offsetBy: offset)], at: answer.startIndex)
        offset -= 1
    }
    var isEqualOtherChar = true
    for offset in abs(offset)...output.count {
        let index = output.index(output.endIndex, offsetBy: -offset)
        let repeatIndex = repeated.index(repeated.endIndex, offsetBy: -offset)
        guard output[index] == repeated[repeatIndex]
        else {
            isEqualOtherChar = false
            break
        }
    }
    // проводим проверку, является ли символ первым в начале repeated
    let isOffsetedIndexIsStartIndex = repeated.index(repeated.endIndex, offsetBy: -output.count) == repeated.startIndex
    if !isEqualOtherChar || (!isOffsetedIndexIsStartIndex && !answer.isEmpty) {
        return mixStrings(repeatedText: repeatText, output: output)
    }
    
    return answer
}

// Решение из разбора. Основная суть - мы просто весь output текст сдвигаем посимвольно относительно повторяемого текста и сравниваем. Условия, что тексты меньше 100 символов позволяют такую роскошь
func getTextToPrintOptimized(repeatText: String, output: String) -> String {
    var ans = 0 // Сколько символов с выходного текста не надо набирать
    for offset in (0...(output.count - 1)).reversed(){
        let outputPosition = offset
        var repeatTextPosition = repeatText.count - 1
        var flag = true
        for position in (0...outputPosition).reversed() {
            let repeatIndexOffset = abs(repeatTextPosition % repeatText.count)
            let outputIndex = output.index(output.startIndex, offsetBy: position)
            let repeatIndex = repeatText.index(repeatText.startIndex, offsetBy: repeatIndexOffset)
            if output[outputIndex] != repeatText[repeatIndex] {
                flag = false
                break
            } else {
                repeatTextPosition -= 1
                // Оператор целочисленного деления в Swift не умеет работать над отрицательными числами, поэтому когда уходит позиция в отрицательные значения, возвращаем позицию в начало
                if repeatTextPosition < 0 { repeatTextPosition = repeatText.count - 1 }
            }
        }
        if flag {
            ans = outputPosition + 1
            break
        }
    }
    let range = output.index(output.startIndex, offsetBy: ans)..<output.endIndex
    return String(output[range])
}

// TODO: - раскомментить
//let input = parseFromLine()
//print(getTextToPrint(repeatText: input.0, output: input.1))

fileprivate struct TestCase {
    let input: (String, String)
    let output: String
}


final class September_Problems_Lessson2A_B_Petya: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // примеры из задания
        .init(input: ("mama", "amamam"), output: "m"),
        .init(input: ("ura", "mura"), output: "mura"),
        .init(input: ("computer", "comp"), output: "comp"),
        .init(input: ("ejudge", "judge"), output: ""),
        .init(input: ("m", "mmm"), output: ""),
        .init(input: ("abccba", "cbaf"), output: "f"),
        .init(input: ("param","paramparamparamparampampam"), output: "pampam"),
        .init(input: ("repeatitasmanytimesasyoumay","itasmanytimesasyoucan"), output: "itasmanytimesasyoucan"),
    ]
    
    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = getTextToPrint(repeatText: currentCase.input.0, output: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testOptimized() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = getTextToPrintOptimized(repeatText: currentCase.input.0, output: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testMixedMethod() {
        XCTAssertEqual(mixStrings(repeatedText: "bcacac", output: "acacg"), "g")
        XCTAssertEqual(mixStrings(repeatedText: "ter", output: "erc"), "c")
        XCTAssertEqual(mixStrings(repeatedText: "ab", output: "abababababadf"), "adf")
        XCTAssertEqual(mixStrings(repeatedText: "ab", output: "cf"), "cf")
        XCTAssertEqual(mixStrings(repeatedText: "param", output: "paramparamparamparampampam"), "pampam")
    }
}
