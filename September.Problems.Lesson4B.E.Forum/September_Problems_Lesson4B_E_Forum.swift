//
//  September_Problems_Lesson4B_E_Forum.swift
//  September.Problems.Lesson4B.E.Forum
//
//  Created by Sergei Popyvanov on 20.10.2021.
//

import XCTest

// Задача E. Форум https://contest.yandex.ru/contest/28970/problems/E/

enum ForumMessage {
    case startTopic(title: String, body: String)
    case reply(message: String, toIndex: Int)
}

class Topic {
    let title: String
    var count = 1
    
    init(_ title: String) {
        self.title = title
    }
}

struct Forum {    
    var answer: String {
        guard !topics.isEmpty else { return "" }
        var maxCommentTopic = topics[0]
        for topic in topics {
            if topic.count > maxCommentTopic.count {
                maxCommentTopic = topic
            }
        }
        return maxCommentTopic.title
    }
    
    private var mapToTopic: [Int: Topic] = [:]
    private var currentIndexOfMessage = 0
    private var topics: [Topic] = []
    
    mutating func add(_ message: ForumMessage) {
        currentIndexOfMessage += 1
        switch message {
        case .startTopic(let title, let body):
            startTopic(title: title, body: body)
        case .reply(let message, let index):
            addAnswer(message, to: index)
        }
    }
    
    private mutating func startTopic(title: String, body: String) {
        let topic = Topic(title)
        mapToTopic[currentIndexOfMessage] = topic
        topics.append(topic)
    }
    
    private mutating func addAnswer(_ message: String, to index: Int) {
        mapToTopic[index]!.count += 1
        mapToTopic[currentIndexOfMessage] = mapToTopic[index]
    }
}

func getAnswerFromLine() -> String {
    let countOfMessage = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    guard countOfMessage > 0 else { return "" }
    var forum = Forum()
    for _ in 1...countOfMessage {
        let index = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
        let isNewTopic = index == 0
        if isNewTopic {
            let title = readLine()!
            let body = readLine()!
            forum.add(ForumMessage.startTopic(title: title, body: body))
        } else {
            let message = readLine()!
            forum.add(ForumMessage.reply(message: message, toIndex: index))
        }
    }
    
    return forum.answer
}

// Раскомментить
//print(getAnswerFromLine())

// MARK - Testing

fileprivate struct TestCase {
    let input: [ForumMessage]
    let output: String
}

final class September_Problems_Lesson4B_E_Forum: XCTestCase {
    
    var sut: Forum!
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(
            input: [
                .startTopic(title: "Олимпиада по информатике", body: "Скоро третья командная олимпиада?"),
                .startTopic(title: "Новая компьютерная игра", body: "Вышла новая крутая игра!"),
                .reply(message: "Она пройдет 24 ноября", toIndex: 1),
                .reply(message: "В Санкт-Петербурге и Барнауле", toIndex: 1),
                .reply(message: "Где найти?", toIndex: 2),
                .reply(message: "Примет участие более 50 команд", toIndex: 4),
                .reply(message: "Интересно, какие будут задачи", toIndex: 6)],
            output: "Олимпиада по информатике"),
        TestCase(input: [.startTopic(title: "topic 1", body: "body 1")], output: "topic 1"),
        // Краевые
        TestCase(input: [.startTopic(title: "Theme 1", body: "start 1"),
                         .startTopic(title: "Theme 2", body: "start 2")],
                 output: "Theme 1")
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            sut = Forum()
            for message in currentCase.input {
                sut.add(message)
            }
            XCTAssertEqual(sut.answer, currentCase.output, "case number \(index) not passed")
        }
    }
    
    func testMapping() {
        let topic1 = Topic("topic 1")
        let topic2 = Topic("topic 2")
        
        var dict: [Int: Topic] = [:]
        dict[1] = topic1
        dict[2] = topic2
        dict[3] = topic1
        dict[4] = topic2
        
        dict[1]!.count += 1
        dict[3]!.count += 1
        dict[4]!.count += 1
        
        XCTAssertEqual(topic1.count, 3)
        XCTAssertEqual(topic2.count, 2)
        XCTAssertEqual(dict[1]!.count, 3)
        
        let arr = [topic1, topic2]
        arr[0].count += 1
        let arr1 = arr
        XCTAssertEqual(arr1[0].count, 4)
    }
    
    func testReadLine() {
        let inputReadLine = "1\n0\ntopic 1\nbody 1"
        let expectedResult = "topic 1"
        withStdinReadingString(inputReadLine) {
            let answer = getAnswerFromLine()
            XCTAssertEqual(answer, expectedResult)
        }
    }
}
