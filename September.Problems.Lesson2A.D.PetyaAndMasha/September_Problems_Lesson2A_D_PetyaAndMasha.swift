//
//  September_Problems_Lesson2A_D_PetyaAndMasha.swift
//  September.Problems.Lesson2A.D.PetyaAndMasha
//
//  Created by Sergei Popyvanov on 04.10.2021.
//

import XCTest

// Задание D. Петя, Маша и веревочки https://contest.yandex.ru/contest/28736/problems/D/

func parseFromLine() -> [Int] {
    _ = readLine()
    let strings = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return strings
}

func smallestStringTakenByMasha(strings: [Int]) -> Int {
    var max = 0
    var sum = 0
    var isOneOrMoreMax = false
    for string in strings {
        if string > max {
            max = string
            isOneOrMoreMax = false
        } else if string == max {
            isOneOrMoreMax = true
        }
        sum += string
    }
    guard !isOneOrMoreMax && sum - max < max else { return sum }
    return max - (sum - max)
}

// TODO: - раскомментить
//let input = parseFromLine()
//print(smallestStringTakenByMasha(strings: input))

// MARK: - Test

fileprivate struct TestCase {
    let input: [Int]
    let output: Int
}

final class September_Problems_Lesson2A_D_PetyaAndMasha: XCTestCase {

    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        .init(input: [1, 5, 2, 1], output: 1),
        .init(input: [5, 12, 4, 3], output: 24),
        .init(input: [1, 5, 5, 1], output: 12),
        .init(input: [3, 3, 6, 3], output: 15),
        .init(input: [3, 3, 50, 3], output: 41),
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = smallestStringTakenByMasha(strings: currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testReadLine() {
        let inputReadLine = "4\n1 5 2 1"
        let expectedResult = 1
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = smallestStringTakenByMasha(strings: input)
            XCTAssertEqual(result, expectedResult)
        }
    }
}
