//
//  September_Problems_A_Interactor.swift
//  September.Problems.A.Interactor
//
//  Created by Sergei Popyvanov on 04.09.2021.
//

import XCTest

// Задача А. Interactor https://contest.yandex.ru/contest/28730/problems/

//Вердикт чекера и вердикт интерактора — это целые числа от 0 до 7 включительно.
// Код завершения задачи — это целое число от -128 до 127 включительно.

func parseFromLine() -> (Int, Int, Int) {
    let resumeCode = Int(readLine()!)!
    let interactorVerdict = Int(readLine()!)!
    let checkerVerdict = Int(readLine()!)!
    return (resumeCode, interactorVerdict, checkerVerdict)
}

func getSystemVerdict(resumeCodeOfTask: Int, interactorVerdict: Int, checkerVerdict: Int) -> Int {
    switch (resumeCodeOfTask, interactorVerdict, checkerVerdict) {
    case (_, interactorVerdict, _) where interactorVerdict == 0:
        return resumeCodeOfTask != 0 ? 3 : checkerVerdict
    case (_, interactorVerdict, _) where interactorVerdict == 1:
        return checkerVerdict
    case (_, interactorVerdict, _) where interactorVerdict == 4:
        return resumeCodeOfTask != 0 ? 3 : 4
    case (_, interactorVerdict, _) where interactorVerdict == 6:
        return 0
    case (_, interactorVerdict, _) where interactorVerdict == 7:
        return 1
    default:
        return interactorVerdict
    }
}

// TODO: убрать комментарии
//let input = parseFromLine()
//print(getSystemVerdict(resumeCodeOfTask: input.0, interactorVerdict: input.1, checkerVerdict: input.2))

// MARK: - Tests

fileprivate struct TestCase {
    let input: (resumeCodeOfTask: Int, interactorVerdict: Int, checkerVerdict: Int)
    let output: Int
}

final class September_Problems_A_Interactor: XCTestCase {

    fileprivate let testCases: [TestCase] = [
        // Тестовые данные
        TestCase(input: (0, 0, 0), output: 0),
        TestCase(input: (-1, 0, 1), output: 3),
        TestCase(input: (42, 1, 6), output: 6),
        TestCase(input: (44, 7, 4), output: 1),
        TestCase(input: (1, 4, 0), output: 3),
        TestCase(input: (-3, 2, 4), output: 2),
        // Чтобы зайти в каждый if
        TestCase(input: (127, 0, 7), output: 3),
        TestCase(input: (0, 0, 7), output: 7),
        TestCase(input: (0, 4, 7), output: 4),
        TestCase(input: (127, 6, 7), output: 0),
        TestCase(input: (127, 7, 7), output: 1),
        // Краевые, нулевые
        TestCase(input: (-128, 0, 0), output: 3),
        TestCase(input: (-128, 7, 7), output: 1),
        TestCase(input: (127, 0, 0), output: 3),
        TestCase(input: (127, 7, 7), output: 1)
    ]
    
    func testReadLine() {
        let inputReadLine = "127\n0\n7"
        let expectedResult = 3
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getSystemVerdict(resumeCodeOfTask: input.0, interactorVerdict: input.1, checkerVerdict: input.2)
            XCTAssertEqual(result, expectedResult)
        }
    }
    
    func testInputCases() throws {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(getSystemVerdict(
                            resumeCodeOfTask: currentCase.input.resumeCodeOfTask,
                            interactorVerdict: currentCase.input.interactorVerdict,
                            checkerVerdict: currentCase.input.checkerVerdict),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }


}
