//
//  September_Problems_Lesson4B_C_FrequencyAnalyse.swift
//  September.Problems.Lesson4B.C.FrequencyAnalyse
//
//  Created by Sergei Popyvanov on 18.10.2021.
//

import XCTest

// Задача C. Частотный анализ
import Foundation
struct Word: Comparable {
    static func < (lhs: Word, rhs: Word) -> Bool {
        guard lhs.frequency == rhs.frequency else { return lhs.frequency > rhs.frequency}
        return lhs.word < rhs.word
    }
    
    let word: String
    let frequency: Int
}

func sortWordsByFrequency(_ lines: [String]) -> String {
    var answer = ""
    var dict: [String: Int] = [:]
    for line in lines {
        let words = line.split(separator: " ")
        for word in words {
            if dict[String(word)] == nil {
                dict[String(word)] = 0
            }
            dict[String(word)]! += 1
        }
    }
    let words = dict.map { Word(word: $0.key, frequency: $0.value)}.sorted().map { $0.word }
    words.forEach { word in
        answer += "\(word)\n"
    }
    return String(answer.dropLast(1))
}

func readWordsFromFile() -> [String] {
    guard let input = try? String(contentsOfFile: "input.txt") else {
        return []
    }
    let lines = input.split(separator: "\n")
    var result: [String] = []
    for line in lines {
        result.append(String(line))
    }
    return result
}

// Раскомментить
//let input = readWordsFromFile()
//print(sortWordsByFrequency(input))

// MARK: - Testing

fileprivate struct TestCase {
    let input: [String]
    let output: String
}

final class September_Problems_Lesson4B_C_FrequencyAnalyse: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(
            input: ["hi", "hi", "what is your name", "my name is bond", "james bond", "my name is", "damme", "van damme", "claude van damme", "jean claude van damme"],
            output: "damme\nis\nname\nvan\nbond\nclaude\nhi\nmy\njames\njean\nwhat\nyour"),
        TestCase(input: ["ai ai ai ai ai ai ai ai ai ai"], output: "ai")
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = sortWordsByFrequency(currentCase.input)
            XCTAssertEqual(answer, currentCase.output, "case number \(index) not passed")
        }
    }

}
