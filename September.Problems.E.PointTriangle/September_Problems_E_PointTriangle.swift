//
//  September_Problems_E_PointTriangle.swift
//  September.Problems.E.PointTriangle
//
//  Created by Sergei Popyvanov on 07.09.2021.
//

import XCTest

// Задача Е. Точка и треугольник https://contest.yandex.ru/contest/28730/problems/E/

func parseFromLine() -> (Int, (Int, Int)) {
    let legLength = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    let point = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    return (legLength[0]!, (point[0]!, point[1]!))
}

func determinePointLocation(legLength: Int, point: (x: Int, y: Int)) -> Int {
    let isOutOfTriangle = point.x < 0 || point.y < 0 || point.y > (legLength - point.x)

    guard isOutOfTriangle else { return 0 }
    let halfLength = legLength / 2
    switch point {
    case let point where point.x <= halfLength  && point.y <= halfLength:
        return 1 // point A(0,0)
    case let point where point.x > halfLength && point.y <= point.x:
        return 2 // point B(d, 0)
    default:
        return 3 // point C(0, d)
    }
}

func determinePointLocationOptimized(legLength: Int, point: (x: Int, y: Int)) -> Int {
    let isOutOfTriangle = point.x < 0 || point.y < 0 || point.y > (legLength - point.x)
    guard isOutOfTriangle else { return 0 }
    func sumOfSquares(_ x: Int, _ y: Int) -> Int {
        (x * x) + (y * y) // сравнивать квадраты очень бытро
    }
    
    let distances = [
        (sumOfSquares(point.x, point.y), 1),
        (sumOfSquares(point.x - legLength, point.y), 2),
        (sumOfSquares(point.x, point.y - legLength), 3)
    ]
    let min = distances.min(by: { a, b in a.0 < b.0 } )! // массив тюплов у нас уже отсортирован, таким образом выбирается первый, если два значения равны
    return min.1
}

// TODO: Раскомментить
//let input = parseFromLine()
//print(determinePointLocation(legLength: input.0, point: (input.1.0, input.1.1)))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, (Int, Int))
    let output: Int
}

final class September_Problems_E_PointTriangle: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задачи
        TestCase(input: (5, (1, 1)), output: 0),
        TestCase(input: (3, (-1, -1)), output: 1),
        TestCase(input: (4, (4, 4)), output: 2),
        TestCase(input: (4, (2, 2)), output: 0),
        // Краевые
        TestCase(input: (6, (3, -8)), output: 1),
        TestCase(input: (6, (-8, 3)), output: 1),
        TestCase(input: (6, (1, 7)), output: 3),
    ]
    
    func testReadLine() {
        let inputReadLine = "5\n1 1"
        let expectedResult = 0
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = determinePointLocation(legLength: input.0, point: (input.1.0, input.1.1))
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testDetermineMethod() throws {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(determinePointLocation(
                            legLength: currentCase.input.0,
                            point: (currentCase.input.1.0, currentCase.input.1.1)),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    
    func testDetermineMethodOptimized() throws {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(determinePointLocationOptimized(
                            legLength: currentCase.input.0,
                            point: (currentCase.input.1.0, currentCase.input.1.1)),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
