//
//  September_Problems_Lesson2_C_Palindrom.swift
//  September.Problems.Lesson2.C.Palindrom
//
//  Created by Sergei Popyvanov on 14.09.2021.
//

import XCTest

// Задача С. Изготовление палиндромов https://contest.yandex.ru/contest/28738/problems/C/

func parseFromLine() -> String {
    readLine()!.split(separator: Character(" ")).map(String.init).joined()
}

func calculateToPalindromCount(_ inputText: String) -> Int {
    var count = 0
    let charArray = Array(inputText)
    for index in 0..<charArray.count {
        let mirroredIndex = charArray.count - index - 1
        guard
            charArray.indices.contains(mirroredIndex),
            index < mirroredIndex
        else { break }
        if charArray[index] != charArray[mirroredIndex] {
            count += 1
        }
    }
    return count
}

// TODO: расскомментить
//let input = parseFromLine()
//print(calculateToPalindromCount(input))

// MARK: - Test

fileprivate struct TestCase {
    let input: String
    let output: Int
}

final class September_Problems_Lesson2_C_Palindrom: XCTestCase {

    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: "a", output: 0),
        TestCase(input: "ab", output: 1),
        TestCase(input: "cognitive", output: 4),
        // Краевые
        TestCase(input: "abba", output: 0),
        TestCase(input: "sergey", output: 2),
        TestCase(input: "natasha", output: 3)
    ]
    
    func testReadLine() {
        let inputReadLine = "cognitive"
        let expectedResult = 4
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = calculateToPalindromCount(input)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testPalindromMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateToPalindromCount(currentCase.input),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }


}
