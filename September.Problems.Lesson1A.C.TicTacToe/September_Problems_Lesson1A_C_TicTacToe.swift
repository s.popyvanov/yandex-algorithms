//
//  September_Problems_Lesson1A_C_TicTacToe.swift
//  September.Problems.Lesson1A.C.TicTacToe
//
//  Created by Sergei Popyvanov on 16.09.2021.
//

import XCTest

// Задача C. Проверьте правильность ситуации https://contest.yandex.ru/contest/28724/problems/C/

enum Symbol: Int {
    case cross = 1
    case zero = 2
    case empty = 0
}

struct TicTacToeTable {
    
    private let table: [[Symbol]]
    
    init(_ rawTable: [[Int]]) {
        var table: [[Symbol]] = []
        rawTable.forEach { rows in
            var row: [Symbol] = []
            rows.forEach { symbol in
                row.append(Symbol(rawValue: symbol)!)
            }
            table.append(row)
        }
        self.table = table
    }
    
    func countSymbol(_ symbol: Symbol) -> Int {
        var count = 0
        table.forEach { rows in
            rows.forEach { currentSymbol in
                if currentSymbol == symbol { count += 1 }
            }
        }
        return count
    }
    
    var countLineSymbols: (cross: Int, zero: Int) {
        var crossLineCount = 0
        var zeroLineCount = 0
        
        func countLine(_ symbol: Symbol) {
            switch symbol {
            case .cross: crossLineCount += 1
            case .zero: zeroLineCount += 1
            case .empty: break
            }
        }
        // Считаем линии
        table.forEach { rows in
            if isLine(rows[0], rows[1], rows[2]) {
                countLine(rows[0])
            }
        }
        // Считаем столбцы
        for column in 0...2 {
            let column = table.map { $0[column] }
            if isLine(column[0], column[1], column[2]) {
                countLine(column[0])
            }
        }
        // Считаем линии наискосок
        if isLine(table[0][0], table[1][1], table[2][2]) { countLine(table[0][0]) }
        if isLine(table[0][2], table[1][1], table[2][0]) { countLine(table[0][2]) }
        return (crossLineCount, zeroLineCount)
    }
    
    private func isLine(_ symbol1: Symbol, _ symbol2: Symbol, _ symbol3: Symbol) -> Bool {
        symbol1 == symbol2 && symbol2 == symbol3 && symbol1 != .empty
    }
}

func parseFromLine() -> [[Int]] {
    let row1 = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let row2 = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let row3 = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return [row1, row2, row3]
}

func isPossibleSituation(_ table: TicTacToeTable) -> String {
    let countCross = table.countSymbol(.cross)
    let countZero = table.countSymbol(.zero)
    let diff = countCross - countZero
    let countLines = table.countLineSymbols
    switch (countLines.cross + countLines.zero) {
    case 0:
        return diff == 0 || diff == 1 ? "YES" : "NO"
    case 1:
        if countLines.cross == 1 {
            return diff == 1 ? "YES" : "NO"
        } else {
            return diff == 0 ? "YES" : "NO"
        }
    case 2:
        return diff == 1 && countLines.cross == 2 ? "YES" : "NO"
    default: return "NO"
    }
}

// TODO: Раскомментить
//let input = parseFromLine()
//print(isPossibleSituation(TicTacToeTable(input)))

// MARK: - Test

fileprivate struct TestCase {
    let input: [[Int]]
    let output: String
}

final class September_Problems_Lesson1A_C_TicTacToe: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: [
                    [1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]],
                 output: "NO"),
        TestCase(input: [
                    [2, 1, 1],
                    [1, 1, 2],
                    [2, 2, 1]],
                 output: "YES"),
        TestCase(input: [
                    [1, 1, 1],
                    [2, 0, 2],
                    [0, 0, 0]],
                 output: "YES"),
        TestCase(input: [
                    [0, 0, 0],
                    [0, 1, 0],
                    [0, 0, 0]],
                 output: "YES"),
        // Краевые
        TestCase(input: [
                    [1, 0, 0],
                    [0, 1, 0],
                    [0, 0, 1]],
                 output: "NO"),
        TestCase(input: [
                    [0, 0, 0],
                    [0, 0, 0],
                    [0, 0, 0]],
                 output: "YES"),
        TestCase(input: [
                    [0, 0, 0],
                    [0, 2, 0],
                    [0, 0, 0]],
                 output: "NO"),
        TestCase(input: [
                    [1, 1, 1],
                    [2, 2, 2],
                    [0, 0, 0]],
                 output: "NO"),
        TestCase(input: [
                    [1, 0, 0],
                    [1, 2, 2],
                    [1, 2, 0]],
                 output: "NO"),
        TestCase(input: [
                    [2, 2, 1],
                    [2, 2, 1],
                    [1, 1, 1]],
                 output: "YES")
    ]
    
    func testReadLine() {
        let inputReadLine = "1 1 1\n1 1 1\n1 1 1"
        let expectedResult = "NO"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = isPossibleSituation(TicTacToeTable(input))
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let table = TicTacToeTable(currentCase.input)
            
            XCTAssertEqual(isPossibleSituation(table),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
