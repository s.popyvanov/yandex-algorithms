//
//  September_Problems_Lesson3B_C_UniqueElements.swift
//  September.Problems.Lesson3B.C.UniqueElements
//
//  Created by Sergei Popyvanov on 14.10.2021.
//

import XCTest

// Задача С. Уникальные элементы https://contest.yandex.ru/contest/28964/problems/C/

func parseFromLine() -> [Int] {
    readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
}

struct Element: Hashable, Equatable {
    let number: Int
    let count: Int
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(number)
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        lhs.number == rhs.number
    }
}

func getUniqueElements(from list: [Int]) -> [Int] {
    var dict: [Element: Int] = [:]
    for (index, element) in list.enumerated() {
        let currentElement = Element(number: element, count: index)
        if let count =  dict[currentElement] {
            dict[currentElement] = count + 1
        } else {
            dict[currentElement] = 1
        }
    }
    let answer = dict.filter { $0.value == 1}.sorted(by: { $0.key.count < $1.key.count}).map {$0.key.number}
    return answer
}

// Раскомментить
//let input = parseFromLine()
//let answer = getUniqueElements(from: input).map { "\($0)" }.joined(separator: " ")
//print(answer)

// MARK: - Testing

fileprivate struct TestCase {
    let input: [Int]
    let output: [Int]
}

final class September_Problems_Lesson3B_C_UniqueElements: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: [1, 2, 2, 3, 3, 3], output: [1]),
        TestCase(input: [4, 3, 5, 2, 5, 1, 3, 5], output: [4, 2, 1]),
        // Краевые
        TestCase(input: [1, 1, 1], output: []),
        TestCase(input: [2, 1, 2, 2, 1], output: []),
        TestCase(input: [-2, 16, -2, 2, 1, -3], output: [16, 2, 1, -3])
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = getUniqueElements(from: currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testHashable() {
        let set: Set<Element> = Set([Element(number: 1, count: 0), Element(number: 1, count: 1)])
        XCTAssertEqual(set.count, 1)
    }
    
    func testReadLine() {
        let inputReadLine = "4 3 5 2 5 1 3 5"
        let expectedResult = "4 2 1"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getUniqueElements(from: input)
            let resultString = result.map { "\($0)"}.joined(separator: " ")
            XCTAssertEqual(resultString, expectedResult)
        }
    }
}
