//
//  September_Problems_Lesson4B_D_DumaElection.swift
//  September.Problems.Lesson4B.D.DumaElection
//
//  Created by Sergei Popyvanov on 19.10.2021.
//

import XCTest

// Задача   D. Выборы Государственной Думы https://contest.yandex.ru/contest/28970/problems/D/

import Foundation

struct Party: Equatable {
    let name: String
    let votes: Int
    
    init(_ name: String, _ votes: Int) {
        self.name = name
        self.votes = votes
    }
}

final class VoteModel: Comparable {
    static func == (lhs: VoteModel, rhs: VoteModel) -> Bool {
        lhs.remainder == rhs.remainder
    }
    
    static func < (lhs: VoteModel, rhs: VoteModel) -> Bool {
        guard lhs.remainder == rhs.remainder else {
            return lhs.remainder < rhs.remainder
        }
        return lhs.allVotes < rhs.allVotes
    }
    
    /// Количество голосов после первого раунда
    let firstRoundVotes: Int
    /// Дробная часть от деления на первое избирательное число
    let remainder: Double
    /// Все голоса за данную партию
    let allVotes: Int
    /// Название партии
    let name: String
    
    init(name: String, firstRoundVotes: Int, remainder: Double, allVotes: Int) {
        self.firstRoundVotes = firstRoundVotes
        self.remainder = remainder
        self.allVotes = allVotes
        self.name = name
    }
    
    // Дополнительные голоса за партию во время второго тура
    var additionalVotes: Int = 0
    // Все голоса
    var dumaVotes: Int {
        firstRoundVotes + additionalVotes
    }
}

func calculateDeputies(_ input: [Party]) -> [Party] {
    var dict: [String: VoteModel] = [:]
    let allVotes = input.map { $0.votes }.reduce(0, +)
    let firstElectoralNumber = Double(allVotes) / 450.0
    var mandats = 450
    for party in input {
        let votesDouble = Double(party.votes) / firstElectoralNumber
        let firstRoundVotes = Int(votesDouble.rounded(.down))
        mandats -= firstRoundVotes
        dict[party.name] = VoteModel(
            name: party.name,
            firstRoundVotes: firstRoundVotes,
            remainder: votesDouble - Double(firstRoundVotes),
            allVotes: party.votes)
    }
    if mandats != 0 {
        let sorted = dict.values.sorted(by: >)
        var index = 0
        while mandats != 0 && index < sorted.count {
            sorted[index].additionalVotes += 1
            mandats -= 1
            index += 1
        }
    }
    var answer: [Party] = []
    for party in input {
        let model = dict[party.name]!
        answer.append(Party(party.name, model.dumaVotes))
    }
    
    return answer
}

func getPartyFromLine(_ input: String) -> Party {
    let items = input.components(separatedBy: " ")
    let votes = Int(items.last!) ?? 0
    let name = items.dropLast().joined(separator: " ")
    return Party(name, votes)
}

func getInputFromFile() -> [Party] {
    guard let input = try? String(contentsOfFile: "input.txt") else {
        return []
    }
    let lines = input.split(separator: "\n")
    var result: [Party] = []
    for line in lines {
        result.append(getPartyFromLine(String(line)))
    }
    return result
}

func getResultString(from parties: [Party]) -> String {
    var result = ""
    for party in parties {
        result += "\(party.name) \(party.votes)\n"
    }
    return String(result.dropLast(1))
}

// Раскомментить
//let input = getInputFromFile()
//let answer = calculateDeputies(input)
//print(getResultString(from: answer))

// MARK: - Test

fileprivate struct TestCase {
    let input: [Party]
    let output: [Party]
}

final class September_Problems_Lesson4B_D_DumaElection: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(
            input: [Party("Party One", 100000), Party("Party Two", 200000), Party("Party Three", 400000), ],
            output: [Party("Party One", 64), Party("Party Two", 129), Party("Party Three", 257), ]),
        TestCase(
            input: [Party("Party number one", 100), Party("Partytwo", 100)],
            output: [Party("Party number one", 225), Party("Partytwo", 225)]),
        TestCase(
            input: [Party("Party number one", 449), Party("Partytwo", 1)],
            output: [Party("Party number one", 449), Party("Partytwo", 1)])
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = calculateDeputies(currentCase.input)
            let count = answer.map { $0.votes }.reduce(0, +)
            XCTAssertEqual(count, 450, "Error count in \(index)")
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testVoteModel() {
        let model1 = VoteModel(name: "", firstRoundVotes: 64, remainder: 0.5161 , allVotes: 100)
        let model2 = VoteModel(name: "", firstRoundVotes: 128, remainder: 0.5714 , allVotes: 200)
        let model3 = VoteModel(name: "", firstRoundVotes: 257, remainder: 0.1428 , allVotes: 400)
        let model4 = VoteModel(name: "", firstRoundVotes: 777, remainder: 0.1428 , allVotes: 401)
        XCTAssertTrue(model2 > model1)
        XCTAssertTrue(model4 > model3)
    }
    
    func testGetPartyFromLine() {
        let string1 = "Party number one 100"
        let party1exp = Party("Party number one", 100)
        let string2 = "Partytwo 100"
        let party2exp = Party("Partytwo", 100)
        let party1 = getPartyFromLine(string1)
        let party2 = getPartyFromLine(string2)
        XCTAssertEqual(party1, party1exp)
        XCTAssertEqual(party2, party2exp)
    }
    
    func testGetResultString() {
        let parties = [Party("Party number one", 100), Party("Partytwo", 100)]
        let exp = "Party number one 100\nPartytwo 100"
        let result = getResultString(from: parties)
        XCTAssertEqual(result, exp)
    }

}
