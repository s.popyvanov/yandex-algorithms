//
//  September_Problems_Lesson3B_A_CountEquatable.swift
//  September.Problems.Lesson3B.A.CountEquatable
//
//  Created by Sergei Popyvanov on 12.10.2021.
//

import XCTest

// Задача А. Количество совпадающих https://contest.yandex.ru/contest/28964/problems/

func parseFromLine() -> ([Int], [Int]) {
    let numbers1 = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let numbers2 = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (numbers1, numbers2)
}

func countEqual(numbers1: [Int], numbers2: [Int]) -> Int {
    let set = numbers1.count >= numbers2.count ? Set(numbers1) : Set(numbers2)
    var count = 0
    if numbers1.count >= numbers2.count {
        for number in numbers2 {
            count += set.contains(number) ? 1 : 0
        }
    } else {
        for number in numbers1 {
            count += set.contains(number) ? 1 : 0
        }
    }
    return count
}

// Раскомментить
//let input = parseFromLine()
//print(countEqual(numbers1: input.0, numbers2: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: ([Int], [Int])
    let output: Int
}

final class September_Problems_Lesson3B_A_CountEquatable: XCTestCase {

    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: ([1, 3, 2], [4, 3, 2]), output: 2),
        TestCase(input: ([1, 2, 6, 4, 5, 7], [10, 2, 3, 4, 8]), output: 2),
        TestCase(input: ([1, 7, 3, 8, 10, 2, 5], [6, 5, 2, 8, 4, 3, 7]), output: 5),
        // Краевые
        TestCase(input: ([], []), output: 0),
        TestCase(input: ([1, 1, 1], [1, 1, 1]), output: 3),
        TestCase(input: ([1, 1, 2, 2], [1, 1, 1, 2, 2, 3]), output: 4),
    ]
    
    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = countEqual(numbers1: currentCase.input.0, numbers2: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
            
        }
    }
    
    func testReadLine() {
        let inputReadLine = "3 1 2\n1 2 3"
        let expectedResult = 3
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = countEqual(numbers1: input.0, numbers2: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }
}
