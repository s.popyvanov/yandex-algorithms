//
//  September_Problems_Lesson1A_E_AnotherPairOfTriangles.swift
//  September.Problems.Lesson1A.E.AnotherPairOfTriangles
//
//  Created by Sergei Popyvanov on 26.09.2021.
//

import XCTest

// Задание Е. Другая пара треугольников https://contest.yandex.ru/contest/28724/problems/E/

func parseFromLine() -> Int {
    let perimeter = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    return perimeter
}

struct Triangle: Equatable {
    private let lengthA: Int
    private let lengthB: Int
    private let lengthC: Int
    
    init(_ lengthA: Int, _ lengthB: Int, _ lengthC: Int) {
        self.lengthA = lengthA
        self.lengthB = lengthB
        self.lengthC = lengthC
    }
    
    var string: String {
        "\(lengthA) \(lengthB) \(lengthC)"
    }
}


func getPairsOfTriangle(_ perimeter: Int) -> String {
    let maxLengthA = Int( perimeter / 3)
    let maxLengthB = Int( (perimeter - maxLengthA) / 2)
    let maxLengthC = perimeter - maxLengthA - maxLengthB
    guard maxLengthA + maxLengthB > maxLengthC else { return "-1"}
    let minLengthA = perimeter % 2 == 0 ? 2 : 1
    
    let minLengthB = Int( (perimeter - minLengthA) / 2)
    let minLengthC = perimeter - minLengthB - minLengthA
    let maxTriangle = Triangle(maxLengthA, maxLengthB, maxLengthC)
    let minTriangle = Triangle(minLengthA, minLengthB, minLengthC)
    return "\(maxTriangle.string)\n\(minTriangle.string)"
}

// TODO: - раскомментить
//let input = parseFromLine()
//print(getPairsOfTriangle(input))

// MARK: - Test

fileprivate struct TestCase {
    let input: Int
    let output: String
}


final class September_Problems_Lesson1A_E_AnotherPairOfTriangles: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: 3, output: "1 1 1\n1 1 1"),
        TestCase(input: 4, output: "-1"),
        TestCase(input: 5, output: "1 2 2\n1 2 2"),
        TestCase(input: 6, output: "2 2 2\n2 2 2"),
        // Краевые
        TestCase(input: 2, output: "-1"),
        TestCase(input: 9, output: "3 3 3\n1 4 4"),
        TestCase(input: 10, output: "3 3 4\n2 4 4"),
        TestCase(input: 100, output: "33 33 34\n2 49 49")
    ]

    func testMethod()  {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = getPairsOfTriangle(currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "3"
        let expectedResult = "1 1 1\n1 1 1"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getPairsOfTriangle(input)
            XCTAssertEqual(result, expectedResult)
        }
    }

}
