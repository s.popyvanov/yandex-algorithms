//
//  September_Problems_Lesson2A_C_Checkers.swift
//  September.Problems.Lesson2A.C.Checkers
//
//  Created by Sergei Popyvanov on 03.10.2021.
//

import XCTest

// Задание C. Шахматная доска https://contest.yandex.ru/contest/28736/problems/C/

struct Checkerboard: Equatable, Hashable {
    let x: Int
    let y: Int
    
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
}

final class ChessBoardFigure {
    private(set) var perimeter = 0
    private var cuttingCheckerboards: Set<Checkerboard> = []
    
    func cut(_ x: Int, _ y: Int) {
        let cuttingForm = Checkerboard(x, y)
        switch calculateAdjacentCheckerboards(for: cuttingForm) {
        // В зависимости от количества соприкасаемых сторон периметр будет изменяться в 4 случаях на соотв. значение
        case 0:
            perimeter += 4
        case 1:
            perimeter += 2
        case 3:
            perimeter -= 2
        case 4:
            perimeter -= 4
        default:
            break
        }
        cuttingCheckerboards.insert(cuttingForm)
    }
    
    private func calculateAdjacentCheckerboards(for checkerboard: Checkerboard) -> Int {
        var count = 0
        let left = Checkerboard(checkerboard.x - 1, checkerboard.y)
        let right = Checkerboard(checkerboard.x + 1, checkerboard.y)
        let up = Checkerboard(checkerboard.x, checkerboard.y - 1)
        let down = Checkerboard(checkerboard.x, checkerboard.y + 1)
        
        if left.x > 0 {
            count += checkContains(left.x, left.y) ? 1 : 0
        }
        
        if right.x <= 8 {
            count += checkContains(right.x, right.y) ? 1 : 0
        }
        if up.y > 0 {
            count += checkContains(up.x, up.y) ? 1 : 0
        }
        if down.y <= 8 {
            count += checkContains(right.x, right.y) ? 1 : 0
        }
        return count
    }
    
    private func checkContains(_ x: Int, _ y: Int) -> Bool {
        cuttingCheckerboards.contains(Checkerboard(x, y))
    }
}

func parseFromLine() {
    let count = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    let figure = ChessBoardFigure()
    for _ in 0..<count {
        let cuttingFigure = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        figure.cut(cuttingFigure[0], cuttingFigure[1])
    }
    print(figure.perimeter)
}

// TODO: - Раскомментить
//parseFromLine()

final class September_Problems_Lesson2A_C_Checkers: XCTestCase {

    func testExample() throws {
        let desk = ChessBoardFigure()
        desk.cut(1, 1)
        desk.cut(1, 2)
        desk.cut(2, 1)
        XCTAssertEqual(desk.perimeter, 8)
        
        let desk1 = ChessBoardFigure()
        desk1.cut(8, 8)
        XCTAssertEqual(desk1.perimeter, 4)
        
        let desk2 = ChessBoardFigure()
        desk2.cut(1, 1)
        desk2.cut(2, 1)
        desk2.cut(1, 2)
        desk2.cut(2, 2)
        desk2.cut(2, 3)
        desk2.cut(3, 2)
        XCTAssertEqual(desk2.perimeter, 12)
    }
    
    func testReadLine() {
        let inputReadLine = "3\n1 1\n1 2\n2 1"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
        }
    }

}
