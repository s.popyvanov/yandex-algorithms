//
//  September_Problems_Lesson2_B_HousesAndShops.swift
//  September.Problems.Lesson2.B.HousesAndShops
//
//  Created by Sergei Popyvanov on 10.09.2021.
//

import XCTest

// Задача B. Дома и магазины https://contest.yandex.ru/contest/28738/problems/B/

func parseFromLine() -> ([Int]) {
    readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
}

enum Building: Int {
    case office = 0
    case house = 1
    case shop = 2
}

// Вариант на связанных списках

final class BuildingNode {
    let buildingType: Building
    let buildingIndex: Int
    weak var previousBuilding: BuildingNode?
    var nextBuilding: BuildingNode?
    
    init(_ buildingType: Building, index: Int) {
        buildingIndex = index
        self.buildingType = buildingType
    }
    
    func getNearestShopDistance() -> Int? {
        var next = nextBuilding
        var nextCount = 0
        while next != nil {
            nextCount += 1
            next = next?.buildingType == .shop ? nil : next?.nextBuilding
        }
        var previousCount = 0
        var previous = previousBuilding
        while previous != nil {
            previousCount += 1
            if previous?.buildingType == .shop || (previousCount > nextCount && nextCount > 0) {
                previous = nil
            } else {
                previous = previous?.previousBuilding
            }
        }
        switch (nextCount, previousCount) {
        case (let nextCount, _) where nextCount == 0:
            return previousCount
        case (_, let previousCount) where previousCount == 0:
            return nextCount
        case (let nextCount, let previousCount):
            return min(nextCount, previousCount)
        }
    }
}

final class Buildings {
    let buildings: [BuildingNode]
    
    init(_ buildings: [Int]) {
        var buildingNodes: [BuildingNode] = []
        for (index, building) in buildings.enumerated() {
            buildingNodes.append(BuildingNode(Building(rawValue: building)!, index: index))
        }
        self.buildings = buildingNodes
        for index in 0..<buildings.count {
            let currentBuilding = self.buildings[index]
            if self.buildings.indices.contains(index + 1) {
                let nextBuilding = self.buildings[index+1]
                currentBuilding.nextBuilding = nextBuilding
            }
            if self.buildings.indices.contains(index - 1) {
                let previousBuilding = self.buildings[index - 1]
                currentBuilding.previousBuilding = previousBuilding
            }
        }
    }
    
}

func getMaxDistanceToShop(for buildings: [Int]) -> Int {
    var max = 1
    let buildingNodes = Buildings(buildings).buildings
    for buildingNode in buildingNodes {
        guard buildingNode.buildingType == .house,
              let minShopDistance = buildingNode.getNearestShopDistance(),
              minShopDistance > max
              else { continue }
        max = minShopDistance
    }
    return max
}

// Оптимизированный вариант из лекции. Основной тип задач максимизация минимума/ минимизация максимума

func getMaxDistanceToShopOptimized(buildings: [Int]) -> Int {
    var leftMostShopIndex = -buildings.count // Индекс самого левого магазина, первое значение - индекс больше чем счетчик массива, чтобы до него дойти было всегда заведомо больше, чем между самыми крайними точками массива = длине массива
    var distanceToShop: [Int] = Array.init(repeating: 0, count: buildings.count) // Создаем массив равный объему входящего массива. В нем будут записываться расстояния от текущего дома до ближайшего слева магазина. Индексы зданий таким образом соответствуют массиву зданий на входе.
    for (index, building) in buildings.enumerated() {
        switch building {
        case Building.shop.rawValue:
            leftMostShopIndex = index // нашли самый левый магазин, заменили мнимый индекс
        case Building.house.rawValue:
            distanceToShop[index] = index - leftMostShopIndex // записали расстояние до ближайшего магазина = разнице индексов. В случае, если слева не было магазина, мы получим расстояние между крайними точками массива
        default:
            continue
        }
    }
    var ans = 0
    var rightShop = 2 * buildings.count // Аналогично самому левому магазину
    for index in 0..<buildings.count {
        let backIndex = buildings.count - index - 1 // Обратный индекс, идем с конца массива
        switch buildings[backIndex] {
        case Building.shop.rawValue:
            rightShop = backIndex
        case Building.house.rawValue:
            // Вот тут как раз пример как в одно действие производится максимизация минимумов
            let minDist = min(rightShop - backIndex, distanceToShop[backIndex]) // выбирается минимальное расстояние между найденным расстоянием до магазина справа и посчитанным расстоянием для этого же дома для магазина слева
            ans = max(minDist, ans) // перезаписываем максимальное значение для ответа
        default:
            continue
        }
    }
    
    return ans
}

// TODO: - раскомментировать
//let input = parseFromLine()
//print(getMaxDistanceToShopOptimized(buildings: input))

// MARK: - Test

fileprivate struct TestCase {
    let input: [Int]
    let output: Int
}

final class September_Problems_Lesson2_B_HousesAndShops: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Из задания
        TestCase(input: [2, 0, 1, 1, 0 ,1 ,0 ,2, 1, 2], output: 3),
        // Краевые
        TestCase(input: [2, 0, 0, 0, 0 ,0 ,0 ,0, 0, 1], output: 9),
        TestCase(input: [1, 0, 0, 0, 0 ,0 ,0 ,0, 0, 2], output: 9)
    ]
    
    func testReadLine() {
        let inputReadLine = "2 0 1 1 0 1 0 2 1 2"
        let expectedResult = 3
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getMaxDistanceToShopOptimized(buildings: input)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testExample() throws {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(getMaxDistanceToShop(for: currentCase.input),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    
    func testOptimized() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(getMaxDistanceToShopOptimized(buildings: currentCase.input),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
