//
//  September_Problems_Lesson2A_A_Confuse.swift
//  September.Problems.Lesson2A.A.Confuse
//
//  Created by Sergei Popyvanov on 01.10.2021.
//

import XCTest

// Задание А. Забавный конфуз https://contest.yandex.ru/contest/28736/problems/

func parseFromLine() -> ([Int], Int) {
    let firstLine = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    let values = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (values, firstLine[1])
}

func calculateConfuse(values: [Int], confuseCount: Int) -> Int {
    var max = values[0]
    var min = values[1]
    for value in values {
        switch value {
        case let value where value < min:
            min = value
        case let value where value > max:
            max = value
        default:
            continue
        }
    }
    return max - min
}

// TODO: - раскомментировать
//let input = parseFromLine()
//print(calculateConfuse(values: input.0, confuseCount: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: ([Int], Int)
    let output: Int
}

final class September_Problems_Lesson2A_A_Confuse: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        .init(input: ([45, 52, 47, 46], 2), output: 7),
        .init(input: ([1, 1, 2, 2], 3), output: 1),
        .init(input: ([-2, -3], 4), output: 1)
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = calculateConfuse(
                values: currentCase.input.0,
                confuseCount: currentCase.input.1)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
}
