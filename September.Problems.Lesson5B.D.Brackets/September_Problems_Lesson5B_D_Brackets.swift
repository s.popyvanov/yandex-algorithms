//
//  September_Problems_Lesson5B_D_Brackets.swift
//  September.Problems.Lesson5B.D.Brackets
//
//  Created by Sergei Popyvanov on 30.10.2021.
//

import XCTest

// Задача D. Правильная, круглая, скобочная https://contest.yandex.ru/contest/29075/problems/D/

func parseFromLine() -> String {
    readLine()!
}

func isCorrectSequence(_ sequence: String) -> Bool {
    guard !sequence.isEmpty else { return false }
    var count = 0
    for char in sequence {
        switch char {
        case (let bracket) where bracket == "(":
            count += 1
        case (let bracket) where bracket == ")":
            guard count > 0 else { return false }
            count -= 1
        default:
            return false
        }
    }
    return count == 0
}

// Раскомментить
let input = parseFromLine()
//print(isCorrectSequence(input) ? "YES" : "NO")

// MARK: - Testing

fileprivate struct TestCase {
    let input: String
    let output: Bool
}

final class September_Problems_Lesson5B_D_Brackets: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: "(())()", output: true),
        TestCase(input: "(()))()", output: false),
        // Краевые
        TestCase(input: "", output: false),
        TestCase(input: "(", output: false),
        TestCase(input: ")", output: false),
        TestCase(input: "((((()))))", output: true),
        TestCase(input: "(()())", output: true),
        TestCase(input: "(()(()))", output: true),
        TestCase(input: "(()", output: false),
        TestCase(input: "())", output: false),
        TestCase(input: "()a", output: false),
        TestCase(input: ")(", output: false),
        TestCase(input: ")(())(", output: false),
        TestCase(input: "())()", output: false),
    ]

    func testExample()  {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = isCorrectSequence(currentCase.input)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "()"
        let expectedResult = "YES"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = isCorrectSequence(input) ? "YES" : "NO"
            XCTAssertEqual(result, expectedResult)
        }
    }
}
