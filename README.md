# Yandex Algorithms
Итого: 4610 (Цель: 10 000)

## Wiki

- [Конспекты лекций и материалы курса](https://gitlab.com/s.popyvanov/yandex-algorithms/-/wikis/Algoritms)
- [Навигатор по алгоритмическим задачам](https://gitlab.com/s.popyvanov/yandex-algorithms/-/wikis/Navigator)


## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 1(A)
### Задание А. Сложное уравнение
- [Задание](https://contest.yandex.ru/contest/28724/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson1A.A.ComplexEquation/September_Problems_Lesson1A_A_ComplexEquation.swift)

### Задание В. Параллелограмм
- [Задание](https://contest.yandex.ru/contest/28724/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson1A.B.Parallelogram/September_Problems_Lesson1A_B_Parallelogram.swift)

### Задание C. Проверьте правильность ситуации
- [Задание](https://contest.yandex.ru/contest/28724/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson1A.C.TicTacToe/September_Problems_Lesson1A_C_TicTacToe.swift)

### Задание D. Футурама
- [Задание](https://contest.yandex.ru/contest/28724/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson1A.D.Futurama/September_Problems_Lesson1A_D_Futurama.swift)

### Задание E. Another Pair Of Triangles
- [Задание](https://contest.yandex.ru/contest/28724/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson1A.E.AnotherPairOfTriangles/September_Problems_Lesson1A_E_AnotherPairOfTriangles.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 2(A)
### Задание А. Забавный конфуз
- [Задание](https://contest.yandex.ru/contest/28736/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2A.A.Confuse/September_Problems_Lesson2A_A_Confuse.swift)

### Задание В. Изобретательный Петя
- [Задание](https://contest.yandex.ru/contest/28736/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lessson2A.B.Petya/September_Problems_Lessson2A_B_Petya.swift)

### Задание C. Шахматная доска
- [Задание](https://contest.yandex.ru/contest/28736/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2A.C.Checkers/September_Problems_Lesson2A_C_Checkers.swift)

### Задание D. Петя, Маша и верёвочки
- [Задание](https://contest.yandex.ru/contest/28736/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2A.D.PetyaAndMasha/September_Problems_Lesson2A_D_PetyaAndMasha.swift)

### Задание E. Газон
- [Задание](https://contest.yandex.ru/contest/28736/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2A.E.Gazon/September_Problems_Lesson2A_E_Gazon.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 1(В)
### Задание А. Интерактор
- [Задание](https://contest.yandex.ru/contest/28730/problems/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.A.Interactor/September_Problems_A_Interactor.swift)

### Задание В. Кольцевая линия метро
- [Задание](https://contest.yandex.ru/contest/28730/problems/B/?success=52632315#30404/2021_08_19/XtbSYa381d)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.B.CircleMetro/September_Problems_B_CircleMetro.swift)

### Задание С. Даты
- [Задание](https://contest.yandex.ru/contest/28730/problems/C/?success=52633715#7/2019_12_14/Q13Dgfydvk)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.C.Dates/September_Problems_C_Dates.swift)

### Задание D. Строительство школы
- [Задание](https://contest.yandex.ru/contest/28730/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.D.SchoolBuildings/September_Problems_D_SchoolBuildings.swift)

### Задание E. Точка и треугольник
- [Задание](https://contest.yandex.ru/contest/28730/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.E.PointTriangle/September_Problems_E_PointTriangle.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 2(В)
### Задание A. Количество равных максимальному
- [Задание](https://contest.yandex.ru/contest/28738/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2.A.NumberEqualsToMaximum/September_Problems_Lesson2_A_NumberEqualsToMaximum.swift)

### Задание B. Дома и магазины
- [Задание](https://contest.yandex.ru/contest/28738/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2.B.HousesAndShops/September_Problems_Lesson2_B_HousesAndShops.swift)

### Задание C. Изготовление палиндромов
- [Задание](https://contest.yandex.ru/contest/28738/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2.C.Palindrom/September_Problems_Lesson2_C_Palindrom.swift)

### Задание D. Лавочки в атриуме
- [Задание](https://contest.yandex.ru/contest/28738/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2.D.AtriumBenches/September_Problems_Lesson2_D_AtriumBenches.swift)

### Задание E. Дипломы в папках
- [Задание](https://contest.yandex.ru/contest/28738/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson2.E.Diploms/September_Problems_Lesson2_E_Diploms.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 3(В)
### Задание А. Количество совпадающих
- [Задание](https://contest.yandex.ru/contest/28964/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson3B.A.CountEquatable/September_Problems_Lesson3B_A_CountEquatable.swift)

### Задание В. Встречалось ли число раньше
- [Задание](https://contest.yandex.ru/contest/28964/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson3B.B.WasThereNumberBefore/September_Problems_Lesson3B_B_WasThereNumberBefore.swift)

### Задание C. Уникальные элементы
- [Задание](https://contest.yandex.ru/contest/28964/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson3B.C.UniqueElements/September_Problems_Lesson3B_C_UniqueElements.swift)

### Задание D. Уникальные элементы
- [Задание](https://contest.yandex.ru/contest/28964/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson3B.D.GuessNumber/September_Problems_Lesson3B_D_GuessNumber.swift)

### Задание E. Автомобильные номера
- [Задание](https://contest.yandex.ru/contest/28964/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson3B.E.CarNumber/September_Problems_Lesson3B_E_CarNumber.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 4(В)
### Задание A. Толя-Карп и новый набор структур, часть 2
- [Задание](https://contest.yandex.ru/contest/28970/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson4B.A.TolyaCarp/September_Problems_Lesson4B_A_TolyaCarp.swift)

### Задание B. Выборы в США
- [Задание](https://contest.yandex.ru/contest/28970/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.4B.B.ElectionUSA/September_Problems_4B_B_ElectionUSA.swift)

### Задание C. Частотный анализ
- [Задание](https://contest.yandex.ru/contest/28970/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson4B.C.FrequencyAnalyse/September_Problems_Lesson4B_C_FrequencyAnalyse.swift)

### Задание D. Выборы Государственной Думы
- [Задание](https://contest.yandex.ru/contest/28970/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson4B.D.DumaElection/September_Problems_Lesson4B_D_DumaElection.swift)

### Задание E. Форум
- [Задание](https://contest.yandex.ru/contest/28970/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson4B.E.Forum/September_Problems_Lesson4B_E_Forum.swift)

## Яндекс. Тренировки по алгоритмам сентябрь 2021, занятие 5(В)
### Задание A. Префиксные суммы
- [Задание](https://contest.yandex.ru/contest/29075/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson5B.PrefixSums/September_Problems_Lesson5B_PrefixSums.swift)

### Задание B. Максимальная сумма
- [Задание](https://contest.yandex.ru/contest/29075/problems/B/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson5B.B.MaxSum/September_Problems_Lesson5B_B_MaxSum.swift)

### Задание C. Каждому по компьютеру
- [Задание](https://contest.yandex.ru/contest/29075/problems/C/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson5B.C.ComputerForEach/September_Problems_Lesson5B_C_ComputerForEach.swift)

### Задание D. Правильная, круглая, скобочная
- [Задание](https://contest.yandex.ru/contest/29075/problems/D/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson5B.D.Brackets/September_Problems_Lesson5B_D_Brackets.swift)

### Задание E. Сумма трёх
- [Задание](https://contest.yandex.ru/contest/29075/problems/E/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Problems.Lesson5B.E.SumOfThree/September_Problems_Lesson5B_E_SumOfThree.swift)

## Яндекс. Тренировки по алгоритмам июнь 2021, занятие 1
### Задание А. Кондиционер
- [Задание](https://contest.yandex.ru/contest/27393/problems/A/)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/June.Problems.A.AirConditioner/June_Problems_A_AirConditioner.swift)

## Практические задания из лекций
### Лекция 1. Сложность. Тестирование. Особые случаи
Сложность алгоритмов на примере поиска максимально повторяющегося символа в строке: O(N^2) -> O(KN) -> O(N)
- [Готовое задание с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/Common.Complexity.Practice/Common_Complexity_Practice.swift)

### Лекция 3. Множества
- [Применение множеств](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.Sets.Practice/September_Sets_Practice.swift)
### Лекция 4. Словари и сортировка подсчётом»
- [Сортировка подсчетом](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.SortingByCount.Dictionary/September_SortingByCount_Dictionary.swift)
### Лекция 5. Префиксные суммы и два указателя
- [Задачи из лекции с тестами](https://gitlab.com/s.popyvanov/yandex-algorithms/-/blob/main/September.PrefixSumm.Pointers.Practice/September_PrefixSumm_Pointers_Practice.swift)


