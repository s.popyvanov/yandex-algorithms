//
//  Common_Complexity_Practice.swift
//  Common.Complexity.Practice
//
//  Created by Sergei Popyvanov on 05.09.2021.
//

import XCTest

//Задача из текстовой строки формата utf8 найти самый частый символ и вывести его на экран. Если одинаковое количество подойдет любой.

/// Перебор - сложность O(N^2) за счет вложенного цикла
func bruteForceMostFrequentSymbol(from string: String) -> String {
    var count = 0
    var result: String = ""
    
    string.forEach { char in
        let filteredCount = string.filter { $0 == char }.count
        if filteredCount > count {
            count = filteredCount
            result = String(char)
        }
    }
    return result
}

// Создание Сета зависит линейно от количества элементов и потом один перебор KN + N -> K(N+1) -> сложность (KN)
func getMostFrequentSymbolWithUsingSet(from string: String) -> String {
    let set = Set(string)
    var count = 0
    var result = ""
    set.forEach { char in
        let filteredCount = string.filter { $0 == char }.count
        if filteredCount > count {
            count = filteredCount
            result = String(char)
        }
    }
    return result
}

/// Создание словаря происходит за 1 перебор = N, выбрать максимальное значение в словаре K - константа, которую можно отбросить при больших N. Получается сложность O(N+K) -> O(N)
func getMostFrequentSymbolWithUsingDict(from string: String) -> String {
    var dict: [Character: Int] = [:]
    string.forEach{ char in
        dict[char] = dict[char] == nil
            ? 0
            : dict[char]! + 1
    }
    let max = dict.max { lhs, rhs in
        lhs.value < rhs.value
    }?.key
    if let max = max { return String(max) }
    else { return "" }
}

fileprivate struct TestCase {
    let input: String
    let output: String
}

final class Common_Complexity_Practice: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        TestCase(input: "aaa", output: "a"),
        TestCase(input: "ababa", output: "a"),
        TestCase(input: "423111", output: "1"),
        TestCase(input: "", output: ""),
        TestCase(input: "aaaююю", output: "aю")
    ]

    func testBruteForceMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let result = bruteForceMostFrequentSymbol(from: currentCase.input)
            checkResult(result, currentCase.output, indexOfCase: index)
        }
    }
    
    func testSetMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let result = getMostFrequentSymbolWithUsingSet(from: currentCase.input)
            checkResult(result, currentCase.output, indexOfCase: index)
        }
    }
    
    func testDictMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let result = getMostFrequentSymbolWithUsingDict(from: currentCase.input)
            checkResult(result, currentCase.output, indexOfCase: index)
        }
    }
    
    private func checkResult(_ result: String, _ expected: String, indexOfCase: Int) {
        if let outputChar = result.first {
            XCTAssertTrue(expected.contains(outputChar), "case number \(indexOfCase) not passed. Output is \(outputChar)")
        } else {
            XCTAssertEqual(expected, "", "case nubmer \(indexOfCase) not passed")
        }
    }
}
