//
//  June_Problems_A_AirConditioner.swift
//  June.Problems.A.AirConditioner
//
//  Created by Sergei Popyvanov on 31.08.2021.
//

import XCTest
import CommonModule

/// Условия задачи https://contest.yandex.ru/contest/27393/problems/A/

func parseFromLine() -> (Int, Int, String) {
    let temps = readLine()?.split(separator: Character(" ")).map(String.init).map(Int.init)
    let modes = readLine()?.split(separator: Character(" ")).map(String.init)
    return (temps![0]!, temps![1]!, modes![0])
}

func readStringFromInputFile() -> String {
    let string = try! String(contentsOfFile: "input.txt")
    return string
}

func parseFromString(_ string: String) -> (Int, Int, String) {
    let lines = string.split(separator: "\n")
    let temps = lines[0].split(separator: " ")
    let mode = String(lines[1])
    return (Int(temps[0])!, Int(temps[1])!, mode)
}

func writeStringToFile(_ string: String) {
    try? string.write(toFile: "output.txt", atomically: true, encoding: .utf8)
}

// MARK: - Types

fileprivate enum Mode: String {
    case freeze
    case heat
    case auto
    case fan
}

// MARK: - Main Method

func calculateOutputTemperature(_ temperatureOfRoom: Int, _ setTemperature: Int, mode: String) -> Int {
    guard let mode = Mode.init(rawValue: mode) else { fatalError("Non exepted value") }
    switch mode {
    case .auto:
        return setTemperature
    case .fan:
        return temperatureOfRoom
    case .freeze:
        return setTemperature < temperatureOfRoom
        ? setTemperature
        : temperatureOfRoom
    case .heat:
        return setTemperature > temperatureOfRoom
        ? setTemperature
        : temperatureOfRoom
    }
}

// TODO: Раскомментить 
//let input = parseFromLine()
//print(String(calculateOutputTemperature(input.0, input.1, mode: input.2)))

//let input = readStringFromInputFile()
//let inputData = parseFromString(input)
//writeStringToFile(String(calculateOutputTemperature(inputData.0, inputData.1, mode: inputData.2)))

// MARK: - Tests

fileprivate struct TestCases {
    let input: (inputTemperature: Int, setTemperature: Int, mode: String)
    let output: Int
}


class June_Problems_A_AirConditioner: XCTestCase {

    fileprivate let testCases: [TestCases] = [
        TestCases(input: (10, 20, "heat"), output: 20),
        TestCases(input: (10, 20, "freeze"), output: 10),
        TestCases(input: (-50, 50, "fan"), output: -50),
        TestCases(input: (50, -50, "fan"), output: 50),
        TestCases(input: (-50, 50, "freeze"), output: -50),
        TestCases(input: (50, -50, "freeze"), output: -50),
        TestCases(input: (50, -50, "heat"), output: 50),
        TestCases(input: (-50, 50, "heat"), output: 50),
        TestCases(input: (-50, 50, "auto"), output: 50),
        TestCases(input: (50, -50, "auto"), output: -50),
        TestCases(input: (0, 0, "auto"), output: 0)
    ]
    
    func testReadLine() {
        let inputReadLine = "10 20\nheat"
        let expectedResult = 20
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = calculateOutputTemperature(input.0, input.1, mode: input.2)
            XCTAssertEqual(result, expectedResult)
        }
    }
    
    func testFromFile() {
        let inputFileString = testReadFromFileInputTxt(bundle: Bundle(for: Self.self))
        let expectedResult = 15
        let input = parseFromString(inputFileString)
        let result = calculateOutputTemperature(input.0, input.1, mode: input.2)
        XCTAssertEqual(result, expectedResult)
    }

    func testInputParametres() {
        let correctedInputModes = ["fan", "auto", "heat", "freeze"]
        correctedInputModes.forEach { mode in
            XCTAssertNotNil(Mode.init(rawValue: mode))
        }
    }
    
    func testModes() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(calculateOutputTemperature(
                            currentCase.input.inputTemperature,
                            currentCase.input.setTemperature,
                            mode: currentCase.input.mode),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}

