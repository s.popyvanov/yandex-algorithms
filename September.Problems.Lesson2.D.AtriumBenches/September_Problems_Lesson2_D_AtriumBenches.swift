//
//  September_Problems_Lesson2_D_AtriumBenches.swift
//  September.Problems.Lesson2.D.AtriumBenches
//
//  Created by Sergei Popyvanov on 14.09.2021.
//

import XCTest

// Задача D Лавочки в атриуме https://contest.yandex.ru/contest/28738/problems/D/

func parseFromLine() -> (Int, Int, [Int]) {
    let firstLine = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    let legs = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (firstLine[0]!, firstLine[1]!, legs)
}

func getNotRemovableLegs(length: Int, numberOfLegs: Int, legs: [Int]) -> [Int] {
    let middle = Int(length / 2)
    var leftSupportLeg = legs[0]
    for index in 0..<legs.count {
        let leg = legs[index]
        switch leg {
        case let leg where leg == middle && length % 2 != 0:
            return [leg]
        case let leg where leg < middle:
            leftSupportLeg = leg
        case let leg where leg >= middle:
            return[leftSupportLeg, leg]
        default:
            break
        }
    }
    return [leftSupportLeg]
}

func printArray(_ input: [Int]) {
    var stringResult = String(input[0])
    if input.indices.contains(1) {
        stringResult += " \(input[1])"
    }
    print(stringResult)
}

// TODO: раскомментить
let input = parseFromLine()
let result = getNotRemovableLegs(length: input.0, numberOfLegs: input.1, legs: input.2)
printArray(result)

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, Int, [Int])
    let output: [Int]
}

final class September_Problems_Lesson2_D_AtriumBenches: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: (5, 2, [0, 2]), output: [2]),
        TestCase(input: (13, 4, [1, 4, 8, 11]), output: [4, 8]),
        TestCase(input: (14, 6, [1, 6, 8, 11, 12, 13]), output: [6, 8]),
        // Краевые
        TestCase(input: (8, 4, [0, 3, 4, 7]), output: [3, 4]),
        TestCase(input: (8, 2, [0, 7]), output: [0, 7]),
        TestCase(input: (8, 3, [0, 3, 4]), output: [3, 4]),
        TestCase(input: (8, 3, [3, 4, 8]), output: [3, 4]),
        TestCase(input: (7, 1, [3]), output: [3]),
        TestCase(input: (7, 3, [3, 4, 5]), output: [3]),
        TestCase(input: (7, 4, [0, 1, 2, 3]), output: [3])
    ]
    
    func testReadLine() {
        let inputReadLine = "14 6\n1 6 8 11 12 13"
        let expectedResult = "6 8"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getNotRemovableLegs(length: input.0, numberOfLegs: input.1, legs: input.2)
            var stringResult = String(result[0])
            if result.indices.contains(1) {
                stringResult += " \(result[1])"
            }
            XCTAssertEqual(stringResult, expectedResult)
        }
    }

    func testMethod() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(getNotRemovableLegs(
                            length: currentCase.input.0,
                            numberOfLegs: currentCase.input.1,
                            legs: currentCase.input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }

}
