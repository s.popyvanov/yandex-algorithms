//
//  September_Problems_Lesson5B_E_SumOfThree.swift
//  September.Problems.Lesson5B.E.SumOfThree
//
//  Created by Sergei Popyvanov on 30.10.2021.
//

import XCTest

// Задача E. Сумма трёх https://contest.yandex.ru/contest/29075/problems/E/

func parseFromLine() -> (Int, [Int], [Int], [Int]) {
    let sum = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    var arrayA = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    arrayA.removeFirst()
    var arrayB = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    arrayB.removeFirst()
    var arrayC = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    arrayC.removeFirst()
    return (sum, arrayA, arrayB, arrayC)
}

struct ValueWithIndex: CustomStringConvertible {
    var description: String { "\(value)-\(index)"}
    let value: Int
    let index: Int
    
    init(_ value: Int, _ index: Int) {
        self.value = value
        self.index = index
    }
}

func mapToValueWithIndex(_ array: [Int]) -> [ValueWithIndex] {
    var result: [ValueWithIndex] = []
    for (index, value) in array.enumerated() {
        result.append(ValueWithIndex(value, index))
    }
    let sortedResult = result.sorted(by: { lhs, rhs in
            guard lhs.value == rhs.value else { return lhs.value < rhs.value }
            return lhs.index < rhs.index
        })
    result = []
    for (index, value) in sortedResult.enumerated() {
        if index > 0 && value.value == sortedResult[index - 1].value {
            continue
        }
        result.append(value)
    }
    return result
}

func firstIndicies(to summ: Int, _ arrayA: [Int], _ arrayB: [Int], _ arrayC: [Int]) -> [Int] {
    let sortedA = mapToValueWithIndex(arrayA)
    let sortedB = mapToValueWithIndex(arrayB)
    let sortedC = mapToValueWithIndex(arrayC)
    var indexC = sortedC.count - 1
    var answer: [Int] = []
    for valueA in sortedA {
        indexC = sortedC.count - 1
        for valueB in sortedB {
            while indexC > 0 && valueA.value + valueB.value + sortedC[indexC].value > summ {
                indexC -= 1
            }
            if valueA.value + valueB.value + sortedC[indexC].value == summ {
                if answer.isEmpty {
                    answer = [valueA.index, valueB.index, sortedC[indexC].index]
                    continue
                } else if answer[0] > valueA.index {
                    answer = [valueA.index, valueB.index, sortedC[indexC].index]
                    continue
                } else if answer[0] >= valueA.index && answer[1] >= valueB.index  {
                    answer = [valueA.index, valueB.index, sortedC[indexC].index]
                    continue
                }
            }
        }
    }
    
    return answer
}

// Раскомментить
//let input = parseFromLine()
//let answer = firstIndicies(to: input.0, input.1, input.2, input.3)
//print(answer.isEmpty ? "-1": answer.map { "\($0)" }.joined(separator: " "))

// MARK: - Testing

fileprivate struct TestCase {
    let input: (Int, [Int], [Int], [Int])
    let output: [Int]
}

final class September_Problems_Lesson5B_E_SumOfThree: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        //  Примеры из задания
        TestCase(input: (3, [1, 2], [3, 1], [3, 1]), output: [0, 1, 1]),
        TestCase(input: (10, [5], [4], [3]), output: []),
        TestCase(input: (5, [1, 2, 3, 4], [5, 2, 1], [5, 3, 2, 2]), output: [0, 1, 2]),
    ]
    
    func testFunc() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let answer = firstIndicies(
                to: currentCase.input.0,
                currentCase.input.1,
                currentCase.input.2,
                currentCase.input.3)
            
            XCTAssertEqual(currentCase.output, answer, "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "3\n2 1 2\n2 3 1\n2 3 1"
        let expectedResult = "0 1 1"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = firstIndicies(to: input.0, input.1, input.2, input.3)
            let resultString = result.map { "\($0)"}.joined(separator: " ")
            XCTAssertEqual(resultString, expectedResult)
        }
    }
    
    func testSortedValueWithIndex() {
        // Сортировка по по возрастающим индексам для указателя слева
        let a = [4, 1, 3, 1, 4, 5]
        let sortedA = mapToValueWithIndex(a)
        print(sortedA)
    }
}
