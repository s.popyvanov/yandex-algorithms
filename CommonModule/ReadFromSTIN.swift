//
//  ReadFromSTIN.swift
//  Algorithms
//
//  Created by Sergei Popyvanov on 31.08.2021.
//

import Foundation

/// Общий код по рекомендациям Яндекс контеста для чтения из строки https://yandex.ru/support/contest/examples-stdin-stdout.html
// https://forums.swift.org/t/how-to-read-from-standard-input-during-unit-tests/43237/3
/// Имитация введения данных в консоль
public func withStdinReadingString(_ string: String, _ body: () throws -> Void) rethrows {
    let oldStdin = dup(STDIN_FILENO)
    let pipe = Pipe()
    dup2(pipe.fileHandleForReading.fileDescriptor, STDIN_FILENO)
    pipe.fileHandleForWriting.write(Data(string.utf8))
    try! pipe.fileHandleForWriting.close()

    defer {
        dup2(oldStdin, STDIN_FILENO)
        close(oldStdin)
        try! pipe.fileHandleForReading.close()
    }
    try body()
}

