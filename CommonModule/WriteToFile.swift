//
//  WriteToFile.swift
//  CommonModule
//
//  Created by Sergei Popyvanov on 04.09.2021.
//

import Foundation

public func writeToFile(bundle: Bundle, string: String) {
    guard let path = bundle.path(forResource: "output", ofType: "txt") else { return }
    try! string.write(toFile: path, atomically: true, encoding: .utf8)
}
