//
//  readFromFile.swift
//  CommonModule
//
//  Created by Sergei Popyvanov on 04.09.2021.
//

import Foundation

/// Тестирование чтения из файла для бандла из тестируемого модуля Bundle(for: Self.self)
public func testReadFromFileInputTxt(bundle: Bundle) -> String {
    let path = bundle.path(forResource: "input", ofType: "txt")
    let string = try! String(contentsOfFile: path!, encoding: .utf8)
    return string
}

public func testReadFromFileOutputTxt(bundle: Bundle) -> String {
    let path = bundle.path(forResource: "output", ofType: "txt")
    let string = try! String(contentsOfFile: path!, encoding: .utf8)
    return string
}

// Пример кода чтения из файла, если файла называется input.txt для неограничеенного количества строчек формата String Int
// https://yandex.ru/support/contest/examples-file.html

func readNums() -> [(String, Int)] {
    guard let line = try? String(contentsOfFile: "input.txt") else {
        return []
    }
    let lines = line.split(separator: "\n")
    var result: [(String, Int)] = []
    for line in lines {
        let split = line.split(separator: " ")
        guard split.count == 2 else { continue }
        guard let number = Int(split[1]) else { continue }
        result.append((String(split[0]), number))
    }
    return result
}
