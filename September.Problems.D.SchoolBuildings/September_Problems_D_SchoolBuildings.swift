//
//  September_Problems_D_SchoolBuildings.swift
//  September.Problems.D.SchoolBuildings
//
//  Created by Sergei Popyvanov on 05.09.2021.
//

import XCTest

// Задача С. Строительство школы https://contest.yandex.ru/contest/28730/problems/D/

func parseFromLine() -> (Int, [Int]) {
    let numberOfPupils = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    let coordinatesOfHouse = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
    return (numberOfPupils[0]!, coordinatesOfHouse)
}

func getSchoolCoordinate(numberOfPupils: Int, coordinatesOfHouses: [Int]) -> Int {
    guard coordinatesOfHouses.count != 2 else {
        return Int((coordinatesOfHouses[0] + coordinatesOfHouses[1])/2)
    }
    let index = Int((numberOfPupils/2))
    return coordinatesOfHouses[index]
}

func getSchoolCoordinateOptimized(numberOfPupils: Int, coordinatesOfHouses: [Int]) -> Int {
    coordinatesOfHouses[numberOfPupils/2]
}

// TODO: расскомментить
//let input = parseFromLine()
//print(getSchoolCoordinate(numberOfPupils: input.0, coordinatesOfHouses: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, [Int])
    let output: [Int]
}

final class September_Problems_D_SchoolBuildings: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: (4, [1,2,3,4]), output: [2,3]), //1,5 + 3.5 = 2.5 [2,3]
        TestCase(input: (4, [1,2,3,4,5]), output: [3]), // 1,5 + 4,5 = 3
        TestCase(input: (3, [-1,0,1]), output: [0]), // -1 + 1 = 0
        TestCase(input: (3, [-1,0,3]), output: [0]), // 0.5 + 3 = 2  / 2 - 3 +2+1 = 6 / 0 - 1+0+3 = 4
        TestCase(input: (5, [-1,0,2,3,10]), output: [2]), // 2,5 + 0,5 = 1,5 [1,2] / 1 - 2+1+1+1+9=14/ 2 - 3+2+0+1+8 = 14 // 3 - 4 + 3 + 1+0+7 = 15
        // Краевые
        TestCase(input: (3, [-100,-50,0]), output: [-50]),
        TestCase(input: (4, [0, 1, 2, 100]), output: [2]),
        TestCase(input: (2, [1, 3]), output: [1,2,3]),
        TestCase(input: (2, [1, 4]), output: [1,2,3,4]),
        TestCase(input: (2, [-5, 10]), output: [-4,-3,-2,-1,0,2, 10]),
        TestCase(input: (2, [4, 5]), output: [4,5]),
        TestCase(input: (5, [-5, -3, 80, 165, 500]), output: [80]),
        TestCase(input: (3, [0, 1, 90]), output: [1])
    ]
    
    func testReadLine() {
        let inputReadLine = "2\n1 4"
        let expectedResult = 2
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getSchoolCoordinate(numberOfPupils: input.0, coordinatesOfHouses: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testGetCoordinate() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            let result = getSchoolCoordinate(numberOfPupils: currentCase.input.0, coordinatesOfHouses: currentCase.input.1)
            
            XCTAssertTrue(
                currentCase.output.contains(result) ,
                "cases \(currentCase.input.1) not passed result is \(result) but expected \(currentCase.output)")
        }
    }
    
    func testGetCoordinateOptimized() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            let result = getSchoolCoordinateOptimized(numberOfPupils: currentCase.input.0, coordinatesOfHouses: currentCase.input.1)
            
            XCTAssertTrue(
                currentCase.output.contains(result) ,
                "cases \(currentCase.input.1) not passed result is \(result) but expected \(currentCase.output)")
        }
    }

}

