//
//  September_Problems_Lesson1A_D_Futurama.swift
//  September.Problems.Lesson1A.D.Futurama
//
//  Created by Sergei Popyvanov on 17.09.2021.
//

import XCTest

// Задание D. Футурама https://contest.yandex.ru/contest/28724/problems/D/

// Решение из разбора
func getSwapsOptimized(numberOfHeroes: Int, swaps: [[Int]]) -> [[Int]]{
    var bodies = Array(repeating: 0, count: numberOfHeroes + 1)
    // возвращаемый параметр answer нужен только для тестов
    var answer: [[Int]] = []
    @discardableResult
    /// Замена тел с принтом результата (кого с кем) и возврат разума второго тела
    func bodySwap(_ body1: Int, _ body2: Int) -> Int {
        print("\(body1) \(body2)")
        bodies.swapAt(body1, body2)
        answer.append([body1, body2])
        return bodies[body2]
    }
    for hero in 1...(numberOfHeroes) { bodies[hero] = hero }
    // Производим первоначальные замены 1 2 3 4
    for swap in swaps {
        bodies.swapAt(swap[0], swap[1]) // 2 1 3 4
    }
    // Проходимся по всем героям, исключая последних двух героев
    for hero in 1...(numberOfHeroes - 2) {
        // Если тело на своем месте, просто идем дальше
        if bodies[hero] != hero {
            var currentHero = hero // Сюда помещаем разум героя, который надо вернуть в свое тело
            //
            while bodies[currentHero] != hero {
                // Замена текущего героя с предпоследним
                currentHero = bodySwap(currentHero, numberOfHeroes - 1) // 1 - 3, 3 1 2 4
            }
            // Производим две замены через последнее тело
            currentHero = bodySwap(currentHero, numberOfHeroes) // 2-4 - 3 4 2 1
            currentHero = bodySwap(currentHero, numberOfHeroes) // 1-4 - 1 4 2 3
            // и третья замена через предпоследнее тело
            bodySwap(bodies[numberOfHeroes - 1], numberOfHeroes - 1) // 2-3, 1, 2, 4, 3
        }
    }
    // Может быть такая ситуация, что после всех замен предпоследний и последний герой не на своих местах
    if bodies[numberOfHeroes - 1] == numberOfHeroes {
        bodySwap(numberOfHeroes - 1, numberOfHeroes)
    }
    return answer
}

// Ниже решение до разбора, оставил как напоминание, как можно много сил и ресурсов потратить на решение задачи "в лоб"
// MARK: - Models

/// Тело героя
final class Body {
    /// Номер тела
    let body: Int
    /// Номер разума
    var mind: Int
    init(_ numberOfHeroes: Int) {
        mind = numberOfHeroes
        body = numberOfHeroes
    }
    var isOwnMind: Bool { body == mind }
}

/// Замена
struct Swap: Equatable {
    let body1: Int
    let body2: Int
    init(_ body1: Int, _ body2: Int) {
        self.body1 = body1
        self.body2 = body2
    }
    
    func isChain(for bodyIndiciesSet: Set<Int>) -> Bool {
        bodyIndiciesSet.contains(body1) || bodyIndiciesSet.contains(body2)
    }
}

/// Вспомогательная структура для тестов, для сравнения, была ли использована данная замена в общем списке замен ( замена 1-2 равна замене 2 - 1)
struct Pair: Hashable, Equatable {
    let objectOne: Int
    let objectTwo: Int
    
    init(_ objectOne: Int, _ objectTwo: Int) {
        self.objectOne = objectOne
        self.objectTwo = objectTwo
    }
    
    init(_ swap: Swap) {
        self.objectOne = swap.body1
        self.objectTwo = swap.body2
    }

    static func == (lhs: Self, rhs: Self) -> Bool {
        (lhs.objectOne == rhs.objectOne && lhs.objectTwo == rhs.objectTwo) ||
            (lhs.objectOne == rhs.objectTwo && lhs.objectTwo == rhs.objectOne)
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(objectOne * objectTwo)
    }
}

struct SwapMachine {
    private(set) var heroes: [Int: Body] = [:]
    private var swaps: Set<Pair> = []
    init(_ numberOfHeroes: Int) {
        for hero in 1...numberOfHeroes { heroes[hero] = Body(hero)}
    }
    
    mutating func swap(_ swap: Swap) {
        guard let body1 = heroes[swap.body1], let body2 = heroes[swap.body2] else {
            fatalError("Невозможно поменять тела")
        }
        swaps.insert(Pair(swap))
        swapBodies(body1, body2)
    }
    
    func checkIsExistSwap(_ swap: Swap) -> Bool {
        swaps.contains(Pair(swap))
    }
    
    private func swapBodies( _ body1: Body, _ body2: Body) {
        let buffer = body1.mind
        body1.mind = body2.mind
        body2.mind = buffer
    }
    
    var isOriginal: Bool {
        !heroes.values.contains { $0.isOwnMind == false }
    }
}

/// Список тел, между которыми были произведены замены
func getSwapChainBodyIndex(for swaps: [Swap]) -> [Set<Int>] {
    guard !swaps.isEmpty else { return [] }
    var swaps = swaps
    var chains: [Set<Int>] = []
    var currentChain: Set<Int> = Set()
    func insertSwapToChain(_ swap: Swap) {
        currentChain.insert(swap.body1)
        currentChain.insert(swap.body2)
        if let indexToRemove = swaps.firstIndex(where: { $0 == swap}) {
            swaps.remove(at: indexToRemove)
        }
    }

    while !swaps.isEmpty {
        insertSwapToChain(swaps.first!)
        while !currentChain.isEmpty {
            let filteredSwaps = swaps.filter { $0.isChain(for: currentChain )}
            if filteredSwaps.isEmpty {
                chains.append(currentChain)
                currentChain = []
            } else {
                filteredSwaps.forEach { filteredSwap in
                    insertSwapToChain(filteredSwap)
                }
            }
        }
    }
    
    return chains
}

func parseFromLine() -> (numberOfHeroes: Int, swaps: [[Int]]) {
    let firstLine = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    var count = firstLine[1]!
    var swaps: [[Int]] = []
    repeat {
        let swap = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        swaps.append(swap)
        count -= 1
    } while count > 0
    return (firstLine[0]!, swaps)
}

func getSwaps(numberOfHeroes: Int, swaps: [[Int]]) -> [[Int]] {
    let inputSwaps = swaps
    var swapMachine = SwapMachine(numberOfHeroes)
    for swap in swaps {
        swapMachine.swap(Swap(swap[0], swap[1]))
    }
    guard !swapMachine.isOriginal else { return [] }

    var answer: [[Int]] = []
    let swaps = swaps.map { Swap($0[0], $0[1])}
    let chains = getSwapChainBodyIndex(for: swaps)
    
    // пробуем прямые замены
    for indexOfHero in swapMachine.heroes.keys {
        guard let hero = swapMachine.heroes[indexOfHero],
              !hero.isOwnMind,
              let isOwnMindBody = swapMachine.heroes.values.first(where: { $0.mind == indexOfHero}),
              !swapMachine.checkIsExistSwap(Swap(indexOfHero, isOwnMindBody.body))
        else { continue }
        answer.append([indexOfHero, isOwnMindBody.body])
        swapMachine.swap(Swap(indexOfHero, isOwnMindBody.body))
    }
    
    for chain in chains { // chain - это список тел, между которыми произошли изменения
        let chainArray = chain.sorted().filter { !swapMachine.heroes[$0]!.isOwnMind }
        if chainArray.count > 1 {
            let step1 = [chainArray[0], numberOfHeroes - 1] // 1-4
            let step2 = [chainArray[1], numberOfHeroes] // 2-5
            answer.append(step1)
            answer.append(step2)
            swapMachine.swap(Swap(step1[0], step1[1]))
            swapMachine.swap(Swap(step2[0], step2[1]))
        }
        
        let lastObject = [numberOfHeroes - 1, numberOfHeroes]
        let penultimate = swapMachine.heroes[numberOfHeroes - 1]!
        let last = swapMachine.heroes[numberOfHeroes]!
        var isStepExist = false
        while !(chain.filter({ !swapMachine.heroes[$0]!.isOwnMind }).isEmpty) {
            
            if lastObject.contains(penultimate.mind) && lastObject.contains(last.mind) {
                let chainArray = chain.sorted().filter { !swapMachine.heroes[$0]!.isOwnMind }
                let step1 = [chainArray[0], numberOfHeroes - 1] // 1-4
                let step2 = [chainArray[1], numberOfHeroes] // 2-5
                let allSwaps = inputSwaps + answer
                let choosingStep = [step1, step2].first { !allSwaps.contains($0) }!
                let checkMind = swapMachine.heroes[choosingStep[0]]!.mind
                let checkBodyMind = swapMachine.heroes[checkMind]!.mind
                if checkMind == checkBodyMind {
                    let newStep = [choosingStep[0], checkMind]
                    answer.append(newStep)
                    swapMachine.swap(Swap(newStep[0], newStep[1]))
                    isStepExist = true
                    continue
                }
                answer.append(choosingStep)
                swapMachine.swap(Swap(choosingStep[0], choosingStep[1]))
                isStepExist = true
                continue
            }

            if !lastObject.contains(penultimate.mind) {
                
                let step = (swapMachine.heroes[penultimate.mind]!.body, numberOfHeroes - 1)
                let allSwaps = inputSwaps + answer
                if !allSwaps.contains([step.0, step.1]) {
                    answer.append([step.0, step.1])
                    swapMachine.swap(Swap(step.0, step.1))
                    isStepExist = true
                    continue
                }
            }
            if !lastObject.contains(last.mind) {
                let step = (swapMachine.heroes[last.mind]!.body, numberOfHeroes)
                let allSwaps = inputSwaps + answer
                if !allSwaps.contains([step.0, step.1]) {
                    answer.append([step.0, step.1])
                    swapMachine.swap(Swap(step.0, step.1))
                    isStepExist = true
                    continue
                }
            }
            if !isStepExist, lastObject.contains(last.mind) || lastObject.contains(penultimate.mind) {
                answer.append([numberOfHeroes - 1, numberOfHeroes])
                swapMachine.swap(Swap(numberOfHeroes - 1, numberOfHeroes))
                isStepExist = true
                continue
            }
            isStepExist = false
        }
    }
    if !swapMachine.heroes[numberOfHeroes]!.isOwnMind {
        swapMachine.swap(Swap(numberOfHeroes - 1, numberOfHeroes))
        answer.append([numberOfHeroes - 1, numberOfHeroes])
    }
    return answer
}

func getSwapsString(_ input: [[Int]]) -> String {
    var output = ""
    for (index, swap) in input.enumerated() {
        output += "\(swap[0]) \(swap[1])"
        if index < input.count - 1 { output += "\n"}
    }
    return output
}

// TODO: - Раскомментить
//let input = parseFromLine()
//let resultArray = getSwaps(numberOfHeroes: input.numberOfHeroes, swaps: input.swaps)
//print(getSwapsString(resultArray))

// TODO: - Раскомментить
//let input = parseFromLine()
//let _ = getSwapsOptimized(numberOfHeroes: input.numberOfHeroes, swaps: input.swaps)

// MARK: - Test

final class SwapMachineForTest {
    // Массив, где тело индекс, а значение - разум
    private var minds: [Int] { didSet { print(minds)}}
    private let originalMinds: [Int]
    private var multiplicationOfBodyIndex: Set<Pair> = []
    
    init(_ numberOfHeroes: Int) {
        var minds: [Int] = []
        for hero in 1...numberOfHeroes { minds.append(hero) }
        self.minds = minds
        self.originalMinds = minds
    }
    
    var isOriginal: Bool { originalMinds == minds }
    
    func swap(_ bodyIndex1: Int, _ bodyIndex2: Int) {
        let pair = Pair(bodyIndex1, bodyIndex2)
        print("swap \(bodyIndex1) - \(bodyIndex2)")
        guard !multiplicationOfBodyIndex.contains(pair)  else {
            fatalError("\(pair) этими телами уже менялись")
        }
        minds.swapAt(bodyIndex1 - 1, bodyIndex2 - 1)
        multiplicationOfBodyIndex.insert(pair)
    }
}

fileprivate struct TestCase {
    let input: (numberOfHeroes: Int, numberOfSwap: Int, swaps: [[Int]])
}

final class September_Problems_Lesson1A_D_Futurama: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: (4, 1, [[1, 2]])),
        TestCase(input: (5, 1, [[1, 2]])),
        TestCase(input: (7, 4, [[1, 2], [3, 1], [2, 3], [4, 5]])),
        TestCase(input: (5, 1, [[1, 2], [2, 3]])),
        TestCase(input: (6, 6, [[1, 2], [1, 3], [2, 4], [1, 4], [2, 3], [3, 4]])),
        TestCase(input: (6, 5, [[1, 2], [2, 3], [3, 4], [2, 4], [1, 3]])),
        TestCase(input: (12, 20, [[5, 2], [1, 10], [5, 1], [9, 8], [3, 5], [7,8], [9,5], [6,1], [7,1], [10,4], [5,7], [10,9], [7,4], [8,6], [1,2], [10,3], [6,7], [8,1], [5,4], [6,9]])),
        TestCase(input: (15, 40, [[7, 8], [8, 12], [2, 13], [6, 5], [11, 9], [5,11], [10,3], [12,4], [12,6], [2,9], [10,13], [1,10], [4,9], [10,6], [7,6], [3,8], [4,13], [4,10], [10,8], [1,8], [13, 11], [6, 3], [2, 4], [10, 9], [9, 5], [11,7], [13,1], [4,5], [6,1], [10,12], [5,13], [11, 8], [6, 11], [1, 11], [8,4], [7,10], [7,2], [12,13], [12,3], [3,4]]))
    ]
    
    func testChainMethod() {
        let swaps: [Swap] = [Swap(1, 2), Swap(2, 3), Swap(4,5)]
        let expectedResult: [Set<Int>] = [Set(arrayLiteral: 1,2,3), Set(arrayLiteral: 4,5)]

        let result = getSwapChainBodyIndex(for: swaps)
        XCTAssertEqual(result , expectedResult)
        
        let swaps1: [Swap] = [Swap(1, 2), Swap(2, 3), Swap(3,4), Swap(2, 4), Swap(1, 3)]
        let result1 = getSwapChainBodyIndex(for: swaps1)
        let expectedResult1: [Set<Int>] = [Set(arrayLiteral: 1,2,3,4)]
        XCTAssertEqual(result1 , expectedResult1)
        let swaps3: [Swap] = [Swap(5, 2), Swap(1, 10), Swap(5,1), Swap(9, 8), Swap(3, 5), Swap(7, 8), Swap(9, 5), Swap(6, 1), Swap(7, 1), Swap(10, 4), Swap(5, 7), Swap(10, 9), Swap(7, 4), Swap(8, 6), Swap(1, 2), Swap(10, 3), Swap(6, 7), Swap(8, 1), Swap(5, 4), Swap(6, 9)]
        var result3: [Set<Int>] = []
        result3 = getSwapChainBodyIndex(for: swaps1)
        let expectedResult3: [Set<Int>] = [Set(arrayLiteral: 1,2,3,4)]
        XCTAssertEqual(result3 , expectedResult3)
    }

    func testMethod() throws {
        
        for index in 0..<testCases.count {
            let input = testCases[index].input
            var swaps: [[Int]] = []
            for swap in input.swaps {
                swaps.append(swap)
            }
            print("input swaps is \(swaps)")
            let answer = getSwaps(numberOfHeroes: input.numberOfHeroes, swaps: input.swaps)
            swaps += answer
            print("answer is \(getSwapsString(answer))")
            print("all swaps is \(swaps)")
            XCTAssertTrue(checkAnswer(numbersOfHeroes: input.numberOfHeroes, swaps: swaps), "test \(index) not correct")
        }
    }
    
    func testOptimisedMethod() {
        for index in 0..<testCases.count {
            let input = testCases[index].input
            var swaps: [[Int]] = []
            for swap in input.swaps {
                swaps.append(swap)
            }
            print("input swaps is \(swaps)")
            let answer = getSwapsOptimized(numberOfHeroes: input.numberOfHeroes, swaps: input.swaps)
            swaps += answer
            print("answer is \(getSwapsString(answer))")
            print("all swaps is \(swaps)")
            XCTAssertTrue(checkAnswer(numbersOfHeroes: input.numberOfHeroes, swaps: swaps), "test \(index) not correct")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "4 1\n1 2"
        let expectedResult = "1 3\n2 4\n2 3\n1 4\n3 4"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = getSwaps(numberOfHeroes: input.numberOfHeroes, swaps: input.swaps)
            let resultString = getSwapsString(result)
            print(resultString)
            XCTAssertEqual(resultString, expectedResult)
        }
    }
    
    func testPair() {
        
        for _ in 1...100 {
            var pairs: Set<Pair> = []
            pairs.insert(Pair(1, 2))
            pairs.insert(Pair(2, 1))
            XCTAssertEqual(pairs.count, 1)
            XCTAssertTrue(pairs.contains(Pair(Swap(1, 2))))
        }
        
    }

    
    private func checkAnswer(numbersOfHeroes: Int, swaps: [[Int]]) -> Bool {
        var machine = SwapMachineForTest(numbersOfHeroes)
        swaps.forEach{ swap in
            machine.swap(swap[0], swap[1])
        }
        return machine.isOriginal
    }
}

