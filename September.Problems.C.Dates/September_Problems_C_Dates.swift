//
//  September_Problems_C_Dates.swift
//  September.Problems.C.Dates
//
//  Created by Sergei Popyvanov on 05.09.2021.
//

import XCTest

// Задача C. Даты https://contest.yandex.ru/contest/28730/problems/C/

import Foundation

func parseFromLine() -> (Int, Int, Int) {
    let dateComponents = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init)
    return (dateComponents[0]!, dateComponents[1]!, dateComponents[2]!)
}

/// С использованием Foundation
func checkUnambiguouslyDetermineDateFromComponentsWithFoundation(_ firstComponent: Int, _ secondComponent: Int, year: Int) -> Int {
    guard firstComponent != secondComponent else { return 1 }
    let calendar = Calendar(identifier: .gregorian)
    let componentsEuropean = DateComponents(calendar: calendar, year: year, month: secondComponent, day: firstComponent)
    let componentsAmerican = DateComponents(calendar: calendar, year: year, month: firstComponent, day: secondComponent)
    let checkUnambiguous = componentsAmerican.isValidDate && componentsEuropean.isValidDate
    return checkUnambiguous ? 0 : 1
}

func checkUnambiguouslyDetermineDateFromComponents(_ firstComponent: Int, _ secondComponent: Int, year: Int) -> Int {
    guard firstComponent != secondComponent else { return 1 }
    let checkUnambiguous = firstComponent <= 12 && secondComponent <= 12
    return checkUnambiguous ? 0 : 1
}



// TODO: убрать комменты
//let input = parseFromLine()
//print(checkUnambiguouslyDetermineDateFromComponents(input.0, input.1, year: input.2))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, Int, Int)
    let output: Int
}

final class September_Problems_C_Dates: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Задания из примера
        TestCase(input: (1, 2, 2003), output: 0),
        TestCase(input: (2, 29, 2008), output: 1),
        // Краевые
        TestCase(input: (1, 1, 1970), output: 1),
        TestCase(input: (31, 1, 2020), output: 1),
        TestCase(input: (12, 12, 2008), output: 1),
        TestCase(input: (2, 28, 2008), output: 1),
        TestCase(input: (1, 12, 2008), output: 0),
        TestCase(input: (12, 1, 2008), output: 0)
    ]
    
    func testReadLine() {
        let inputReadLine = "1 2 2003"
        let expectedResult = 0
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = checkUnambiguouslyDetermineDateFromComponentsWithFoundation(input.0, input.1, year: input.2)
            XCTAssertEqual(result, expectedResult)
        }
    }
    
    func testReadLineWithoutFounndation() {
        let inputReadLine = "1 2 2003"
        let expectedResult = 0
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = checkUnambiguouslyDetermineDateFromComponents(input.0, input.1, year: input.2)
            XCTAssertEqual(result, expectedResult)
        }
    }
    
    func testCheckingWithoutFoundation() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(checkUnambiguouslyDetermineDateFromComponents(
                            currentCase.input.0,
                            currentCase.input.1,
                            year: currentCase.input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
    

    func testChecking() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            
            XCTAssertEqual(checkUnambiguouslyDetermineDateFromComponentsWithFoundation(
                            currentCase.input.0,
                            currentCase.input.1,
                            year: currentCase.input.2),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }
}
