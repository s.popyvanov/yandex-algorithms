//
//  September_Problems_Lesson1A_B_Parallelogram.swift
//  September.Problems.Lesson1A.B.Parallelogram
//
//  Created by Sergei Popyvanov on 16.09.2021.
//

import XCTest

// Задача В. Параллелограм https://contest.yandex.ru/contest/28724/problems/B/

struct Point {
    let x: Int
    let y: Int
}

struct Segment {
    let point1: Point
    let point2: Point
}

struct Tetragon {
    let point1: Point
    var point2: Point
    var point3: Point
    var point4: Point
    
    init(from points: [Int]) {
        point1 = Point(x: points[0], y: points[1])
        point2 = Point(x: points[2], y: points[3])
        point3 = Point(x: points[4], y: points[5])
        point4 = Point(x: points[6], y: points[7])
    }
    
    // y = kx + b - уравнение прямой, параллельные линии имеют одинаковое k
    // y0 = kx0 + b, y1 = kx1 + b
    // b = y1 - kx1 -> y0 = kx0 + y1 - kx1
    // k = (y0 - y1) / (x0 - x1)
    // одинаковые отрезки имеют одинаковый квадрат расстояний (x1-x0)^2 + (y1-y0)^2
    // сравниваем отрезки 1-2 и 3-4, 2-3 и 4-1 на паралельность и равность
    // так как точки могут быть в любом порядке, есть только две возможные комбинации, когда заменяем 2 и 3 точку или 3 и 4
    mutating func isParallelogram() -> Bool {
        if checkParallelogram() { return true }
        swap(&point2, &point3)
        if checkParallelogram() { return true }
        swap(&point2, &point3) // обратный свап
        swap(&point3, &point4)
        return checkParallelogram()
    }
    
    private func checkParallelogram() -> Bool {
        let segment1 = (point1, point2)
        let segment2 = (point3, point4)
        let segment3 = (point2, point3)
        let segment4 = (point4, point1)
        guard isParallelLineSegment(segment1, segment2) else { return false }
        guard isParallelLineSegment(segment3, segment4) else { return false }
        guard isEqualLength(segment1, segment2) else { return false }
        guard isEqualLength(segment3, segment4) else { return false }
        return true
    }
    
    var points: [Int] { [point1.x, point1.y, point2.x, point2.y]}
}

func square(_ value: Int) -> Int {
    value * value
}
func isParallelLineSegment(_ segment1: (Point, Point), _ segment2: (Point, Point)) -> Bool {
    // guard для случая если это вертикальные прямые где k->inf
    guard
        (segment1.0.x - segment1.1.x) != 0 && (segment2.0.x - segment2.1.x) != 0
    else {
        return true }
    let k1 = Double(segment1.0.y - segment1.1.y) / Double(segment1.0.x - segment1.1.x)
    let k2 = Double(segment2.0.y - segment2.1.y) / Double(segment2.0.x - segment2.1.x)
    return k1 == k2
}

func squareLength(_ point1: Point, _ point2: Point) -> Int {
    square(point1.x - point2.x) + square(point1.y - point2.y)
}
func isEqualLength(_ segment1: (Point, Point), _ segment2: (Point, Point)) -> Bool {
    squareLength(segment1.0, segment1.1) == squareLength(segment2.0, segment2.1)
}

func parseFromLine() -> (Int, [Tetragon]) {
    let outputCount = readLine()!.split(separator: Character(" ")).map(String.init).map(Int.init).first!!
    var count = outputCount
    var tetragons: [Tetragon] = []
    repeat {
        let points = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        tetragons.append(Tetragon(from: points))
        count -= 1
    } while count > 0
    return (outputCount, tetragons)
}

func checkIsParallelograms(count: Int, tetragons: [Tetragon]) -> String {
    var tetragons = tetragons
    var answers = ""
    for index in 0..<count {
        answers += tetragons[index].isParallelogram() ? "YES" : "NO"
        if index != count - 1 { answers += "\n"}
    }
    return answers
}

// TODO: - Раскомментить
//let input = parseFromLine()
//print(checkIsParallelograms(count: input.0, tetragons: input.1))

// MARK: - Test

fileprivate struct TestCase {
    let input: (Int, [[Int]])
    let output: String
}

final class September_Problems_Lesson1A_B_Parallelogram: XCTestCase {
    
    fileprivate let testCases: [TestCase] = [
        // Пример из задания
        TestCase(input: (3, [
                            [1, 1, 4, 2, 3, 0, 2, 3],
                            [1, 1, 5, 2, 2, 3, 3, 0],
                            [0, 0, 5, 1, 6, 3, 1, 2]]),
                 output: "YES\nNO\nYES"),
        TestCase(input: (2, [
                            [10, 10, 12, 12, 11, 14, 13, 16],
                            [10, 10, 20, 9, 19, 19, 9, 20]]),
                 output: "YES\nYES"),
        TestCase(input: (9, [
            [100, 100, -100, -100, -1, 1, 1, -1],
            [100, 100, -1, 1, 1, -1, -100, -100],
            [100, 100, -1, 1, -100, -100, 1, -1],
            [-10, -10, -14, -14, -11, -12, -12, -13],
            [-10, -10, -14, -14, -12, -13, -11, -12],
            [-10, -10,  -11, -12, -14, -14, -12, -13],
            [-5, -11, -5, -1,  0, 0, 0, -10],
            [-5, -11,  0, 0, -5, -1, 0, -10],
            [-5, -11, -5, -1,  0, -10, 0, 0,]
        ]),
        output: "YES\nYES\nYES\nNO\nNO\nNO\nYES\nYES\nYES")
    ]
    
    func testReadLine() {
        let inputReadLine = "3\n1 1 4 2 3 0 2 3\n1 1 5 2 2 3 3 0\n0 0 5 1 6 3 1 2"
        let expectedResult = "YES\nNO\nYES"
        withStdinReadingString(inputReadLine) {
            let input = parseFromLine()
            let result = checkIsParallelograms(count: input.0, tetragons: input.1)
            XCTAssertEqual(result, expectedResult)
        }
    }

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            let input = currentCase.input
            var tetragons: [Tetragon] = []
            input.1.forEach { points in
                let tetragon = Tetragon(from: points)
                tetragons.append(tetragon)
            }
            
            XCTAssertEqual(checkIsParallelograms(count: input.0, tetragons: tetragons),
                           currentCase.output,
                           "case number \(index) not passed")
        }
    }

}
