//
//  September_Problems_Lesson4B_A_TolyaCarp.swift
//  September.Problems.Lesson4B.A.TolyaCarp
//
//  Created by Sergei Popyvanov on 17.10.2021.
//

import XCTest

// Задача A. Толя-Карп и новый набор структур, часть 2 https://contest.yandex.ru/contest/28970/problems/

struct ParcelGrouper {
    var answer: String {
        var result = ""
        for key in parcelDict.keys.sorted() {
            result += "\(key) \(parcelDict[key]!)\n"
        }
        return String(result.dropLast(1))
    }
    
    private var parcelDict: [Int: Int] = [:]
    
    mutating func addParcel(number: Int, color: Int) {
        if parcelDict[number] == nil {
            parcelDict[number] = 0
        }
        parcelDict[number]! += color
    }
}

func getAnswerFromLine() -> String {
    let parcelNumber = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init).first!
    guard parcelNumber > 0 else { return "" }
    var grouper = ParcelGrouper()
    for _ in 1...parcelNumber {
        let package = readLine()!.split(separator: Character(" ")).map(String.init).compactMap(Int.init)
        grouper.addParcel(number: package[0], color: package[1])
    }
    
    return grouper.answer
}

// Раскомментить
//print(getAnswerFromLine())

// MARK: - Test

fileprivate struct TestCase {
    let input: [(Int, Int)]
    let output: String
}

final class September_Problems_Lesson4B_A_TolyaCarp: XCTestCase {
    
    var sut: ParcelGrouper!
    
    fileprivate let testCases: [TestCase] = [
        // Примеры из задания
        TestCase(input: [(1, 5), (10, -5), (1, 10), (4, -2), (4, 3), (4, 1), (4, 0)],
                 output: "1 15\n4 2\n10 -5"),
        TestCase(input: [(5, -10000), (-5, 100000000000), (10, 2000000000000), (-5, -300000000000), (0, 10000000000000)],
                 output: "-5 -200000000000\n0 10000000000000\n5 -10000\n10 2000000000000"),
        TestCase(input: [], output: "")
    ]

    func testExample() {
        for index in 0..<testCases.count {
            let currentCase = testCases[index]
            sut = ParcelGrouper()
            for parcel in currentCase.input {
                sut.addParcel(number: parcel.0, color: parcel.1)
            }
            XCTAssertEqual(sut.answer, currentCase.output, "case number \(index) not passed")
        }
    }
    
    func testReadLine() {
        let inputReadLine = "3\n1 2\n1 3\n1 -2"
        let expectedResult = "1 3"
        withStdinReadingString(inputReadLine) {
            let answer = getAnswerFromLine()
            XCTAssertEqual(answer, expectedResult)
        }
    }
    
    func testZeroReadLine() {
        let inputReadLine = "0"
        let expectedResult = ""
        withStdinReadingString(inputReadLine) {
            let answer = getAnswerFromLine()
            XCTAssertEqual(answer, expectedResult)
        }
    }

}
